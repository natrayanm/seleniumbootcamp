package week7.cucumber.runner;

import org.testng.annotations.Test;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@Test
@CucumberOptions(features= "src/main/java/week7/cucumber/features", glue="week7/cucumber/steps", monochrome=true, 
				plugin= {"junit:target/cucumber-report.xml", "json:target/cucumber.json", 
						"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:", "pretty"
						}, tags = "@run1")
public class BaseRunner extends AbstractTestNGCucumberTests{
	
	
	}