package week7.cucumber.steps;

import org.openqa.selenium.By;

import io.cucumber.java.en.Given;
import week5pomImplementation.hooks.TestNgHooks;
import week5pominplmentation.base.CommonMethod;
import week5pominplmentation.pages.SalesForceLandingPage;

public class CommonSteps extends TestNgHooks{

	@Given("User clicks the applauncher")
	public void applauncher() throws InterruptedException {
		System.out.println("applauncher");
		System.out.println("Title while landing "+ getDriver().getTitle());
		if(getDriver().getTitle().equals("Home Page ~ Salesforce - Developer Edition")) {
			getDriver().findElement(By.xpath("//div/div/a[@class=\"switch-to-lightning\"]")).click();
			System.out.println("Title after switching "+getDriver().getTitle());

		}
		
		/*try {
			Assert.assertEquals(driver.getTitle(), "Home Page ~ Salesforce - Developer Edition");
		}
		catch(AssertionError e) {
			System.out.println("Exception is caught");
			test.fail("User failed to click the applauncher " + e.toString());
		}*/
		

		//new SalesForceLandingPage(driver, wait, prop)
		new SalesForceLandingPage()
		.clickApplauncher()
		.clickviewAll();

		//test.pass("User clicks the applauncher");

	}
}
