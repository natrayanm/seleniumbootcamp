package week7.cucumber.steps;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import week5pomImplementation.hooks.TestNgHooks;
import week5pominplmentation.pages.SalesForceLoginPage;

public class HooksImplementation extends TestNgHooks{

	ExtentReports extent = new ExtentReports();
	ExtentHtmlReporter avent = new ExtentHtmlReporter("/Reports/");
	
	
	@Before
	public void beforeMethod(Scenario sc) throws IOException {
		System.out.println("Scneario name is " + sc.getName());
		setupBrowser("chrome", "https://login.salesforce.com/", "C:\\Users\\natra\\Downloads\\selenium");
		login("makaia@testleaf.com", "BootcampSel$123");
		sendDataNewCucumber(sc.getName());
		
	}
	
	@After
	public void afterMethod() {
		System.out.println("Shutting down");
		closeBrowser();
		//flushReport();

	}
	
	public void login(String username, String password) {
		//	new SalesForceLoginPage(getDriver(), getWait(), prop).enterUsername(username).enterPassword(password).clickLogin();
			new SalesForceLoginPage().enterUsername(username).enterPassword(password).clickLogin();
			
		}
	
	@SuppressWarnings("unchecked")
	public Map<String, String> sendDataNewCucumber(String scenario) throws IOException {
		String[] names = scenario.split(":");
		//String[] names = str.split(":");
		//System.out.println(names[0]);
		//System.out.println((names[1].split("_"))[1]);

		fileName = "C:\\Natrayan\\Testleaf\\Docs\\"+names[0]+".xlsx";
		sheetName= (names[1].split("_"))[1];


		@SuppressWarnings("resource")
		XSSFWorkbook  wb =  new XSSFWorkbook(fileName);
		XSSFSheet ws = wb.getSheet(sheetName);

		Row row = ws.getRow(0);
		int lastcolumnused = row.getLastCellNum();
		int colnum = 0;
		for (int i = 0; i < lastcolumnused; i++) {
			if (row.getCell(i).getStringCellValue().equalsIgnoreCase("TC-01")) {
				colnum = i;
				break;
			}
		}


		for(int i=1; i<= wb.getSheet(sheetName).getLastRowNum(); i++) {
			row = ws.getRow(i);
			Cell column = row.getCell(colnum);
			String cellValue = column.getStringCellValue();
			System.out.println(cellValue);

			ObjectMapper objectMapper = new ObjectMapper();
			try {    
				dataMap = objectMapper.readValue(cellValue, Map.class);   
			}catch (Exception e) { 
				e.printStackTrace();
			}
			System.out.println("Map is " + dataMap);
			System.out.println("Map Size is " + dataMap.size());
		}

		return dataMap;

	}

	
}
