package week7.cucumber.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import week5pomImplementation.hooks.TestNgHooks;
import week5pominplmentation.pages.SalesForceHomePage;
import week5pominplmentation.pages.SalesForceLandingPage;
import week5pominplmentation.pages.TasksHomePage;

public class TaskSteps extends TestNgHooks{
	
	
	@Given("selects the sales application")
	public void selectSales() throws InterruptedException {
		System.out.println("sales application");
		//new SalesForceLandingPage(driver, wait, prop)
		new SalesForceLandingPage()
		//.clickviewAll()
		.enterAndClickSales();
		
	}
	
	@When("User creates new task")
	public void createNewTasks() throws InterruptedException {
		System.out.println("create new tasks");
		//new SalesForceHomePage(driver, wait, prop)
		new SalesForceHomePage()
		.expandTask()
		.clickNewTask()
		.enterTaskName(dataMap.get("taskName"))
		.selectStatus(dataMap.get("Status"))
		.clickSearchContacts()
		.createNewContact(dataMap.get("name"), dataMap.get("toast"));
		
	}
	
	@Then("new task is created")
	public void checkNewTask() throws InterruptedException {
		System.out.println("new task is created");
		//new SalesForceHomePage(driver, wait, prop).saveNewTask(dataMap.get("toast"));
		new SalesForceHomePage().saveNewTask(dataMap.get("toast"));
	}
	
	@Then("User able to view and assert the new task in recently_viewed item")
	public void viewNewTask() throws InterruptedException {
		System.out.println("New task is created");
		//new SalesForceHomePage(driver, wait, prop).clickTasks();
		//new TasksHomePage(driver, wait, prop).clickRecentlyViewedDropdown()

		new SalesForceHomePage().clickTasks();
		new TasksHomePage().clickRecentlyViewedDropdown()
		
		.selectRecentlyViewed()
		.clickTaskWithName(dataMap.get("taskName"))
		.assertTaskName(dataMap.get("name"))
		.assertTaskSubject(dataMap.get("taskName"));
	}
	
	@Given("selects the recently viewed tasks")
	public void selectRecentlyViewedTask() throws InterruptedException {
		//new SalesForceHomePage(driver, wait, prop)
		new SalesForceHomePage()
		.clickTasks();
		//new TasksHomePage(driver, wait, prop)
		new TasksHomePage()
		.clickRecentlyViewedDropdown()
		.selectRecentlyViewed();
		
	}
	
	@When("User edits the task")
	public void editNewTasks() throws InterruptedException {
		System.out.println("create new tasks");
/*		new SalesForceHomePage(driver, wait, prop)
		.clickTasks();
		new TasksHomePage(driver, wait, prop)
		.clickRecentlyViewedDropdown()
		.selectRecentlyViewed() */
//		new TasksHomePage(driver, wait, prop)
		new TasksHomePage()
		.clickTaskWithName(dataMap.get("taskName"))
		.clickEditDueDate()
		.enterDueDate(dataMap.get("dueDate"))
		.clickEditPriority()
		.selectPriority(dataMap.get("priority"))
		.clickSaveTask();
	}
	
	@Then("User able to view and assert the edited task in recently_viewed item")
	public void checkEditTask() throws InterruptedException {
		//new SalesForceHomePage(driver, wait, prop)
		new SalesForceHomePage()
		.clickTasks();
		//new TasksHomePage(driver, wait, prop)
		new TasksHomePage()
		.clickRecentlyViewedDropdown()
		.selectRecentlyViewed()
		.clickTaskWithName(dataMap.get("taskName"))
		.assertTaskPriority(dataMap.get("priority"));
	}
	
	@When("User deletes the task")
	public void deleteTask() throws InterruptedException {
		//new TasksHomePage(driver, wait, prop)
		new TasksHomePage()
		.clickTaskWithName(dataMap.get("taskName"))
		.clickMoreActionForTask()
		.clickDeleteTask();
	}
	
	@Then("User able to view and assert the task is deleted")
	public void checkDeleteTask() {
		//new TasksHomePage(driver, wait, prop)
		new TasksHomePage()
		.validateToastMsg(dataMap.get("toast"));

	}
	
}
