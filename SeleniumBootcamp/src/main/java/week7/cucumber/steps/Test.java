package week7.cucumber.steps;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.compress.archivers.dump.InvalidFormatException;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Test {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		//String str="Tasks: TC-01_createTask";
		//String str="Tasks: TC-01_editTask";
		String str="Tasks: TC-01_deleteTask";

		String[] names = str.split(":");
		System.out.println(names[0]);
		System.out.println((names[1].split("_"))[1]);
		
		String fileName = "C:\\Natrayan\\Testleaf\\Docs\\"+names[0]+".xlsx";
		String sheetName= (names[1].split("_"))[1];

		Map<String, String> dataMap = new LinkedHashMap<String, String>();
		
		 XSSFWorkbook  wb =  new XSSFWorkbook(fileName);
		XSSFSheet ws = wb.getSheet(sheetName);
		
				Row row = ws.getRow(0);
				// it will give you count of row which is used or filled
				short lastcolumnused = row.getLastCellNum();
				int colnum = 0;
				for (int i = 0; i < lastcolumnused; i++) {
					if (row.getCell(i).getStringCellValue().equalsIgnoreCase("TC-01")) {
						colnum = i;
						break;
					}
				}
				
				
				for(int i=1; i<= wb.getSheet(sheetName).getLastRowNum(); i++) {
					row = ws.getRow(i);
					Cell column = row.getCell(colnum);
					 String cellValue = column.getStringCellValue();
					 System.out.println(cellValue);
					
					 ObjectMapper objectMapper = new ObjectMapper();
					 String json = "{\r\n" + "   \"name\": \"codezup\",\r\n" + " \"stream\": \"computer-science\"\r\n" + "}";
					 try {    
					      // below line is for conversion
						 dataMap = objectMapper.readValue(cellValue, Map.class);   
					 }catch (Exception e) { 
						 e.printStackTrace();
					 }
					 System.out.println("Map is " + dataMap);
					 System.out.println("Map Size is " + dataMap.size());
				}
					
	}	
}
