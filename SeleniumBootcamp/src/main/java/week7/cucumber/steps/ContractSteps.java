package week7.cucumber.steps;

import java.text.ParseException;

import org.testng.ITestContext;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import week5pomImplementation.hooks.TestNgHooks;
import week5pominplmentation.pages.ContractsRecentlyViewedPage;
import week5pominplmentation.pages.SalesForceLandingPage;

public class ContractSteps extends TestNgHooks{
	
	
	@Given("selects the contracts")
	public void selectContract() throws InterruptedException {
		//new SalesForceLandingPage(driver, wait, prop)
		new SalesForceLandingPage()
		.scrollAndClickContracts();
	}

	@When("User creates new contract")
	public void newContract() throws InterruptedException {
		//new ContractsRecentlyViewedPage(driver, wait, prop)
		new ContractsRecentlyViewedPage()
		.clickNewContract()
		.clickSearchAccounts()
		.clickOrCreateAccount(dataMap.get("accName"))
		.enterContractStartDate(dataMap.get("sDate"))
		.enterContractPeriod(dataMap.get("period"))
		.saveContract();
	}
	
	@Then("new contract is created")
	public void checkNewContract() throws InterruptedException {
		//new ContractsRecentlyViewedPage(driver, wait, prop)
		new ContractsRecentlyViewedPage()
		.validateToastMsg(dataMap.get("toast"));
	}
	
	@Then("User able to view and assert the new contract in recently_viewed item")
	public void validateNewContract() throws ParseException, InterruptedException {
		ITestContext context = null;
		//new ContractsRecentlyViewedPage(driver, wait, prop)
		new ContractsRecentlyViewedPage()
		.extractContractId(context)
		.clickNewlyCreatedContract()
		//.closeNewContractTab()
		.loadContractTable()
		.iterateContractTable(dataMap.get("accName"));
		
		//ISuite suite = context.getSuite();
		//contract_id = (String)suite.getAttribute("contract_id");
		System.out.println("contract_id is " +contract_id);

	}
	
	@When("User edits contract")
	public void editContract() throws InterruptedException {
		//new ContractsRecentlyViewedPage(driver, wait, prop)
		new ContractsRecentlyViewedPage()
		.loadContractTable()
		.iterateAndSelectContract(contract_id)
		.clickEditContract()
		.changeContractStatus(dataMap.get("status"))
		.changeExpirationDate(dataMap.get("cEDate"))
		.saveContract();
	}
	
	@Then("contract is updated")
	public void checkUpdateContract() throws InterruptedException {
		//new ContractsRecentlyViewedPage(driver, wait, prop)
		new ContractsRecentlyViewedPage()
		.validateToastMsg(dataMap.get("toast"));
	}
	
	@Then("User able to view and assert the updated contract in recently_viewed item")
	public void validateUpdatedContract() throws InterruptedException {
		//new ContractsRecentlyViewedPage(driver, wait, prop)
		new ContractsRecentlyViewedPage()
		.clickSearchContract()
		.enterContractAndSearch(contract_id)
		.loadContractTable()
		.iterateAndCompareContract(contract_id);
		
	}
	
	@When("User deletes contract")
	public void deleteContract() throws InterruptedException {
		//new ContractsRecentlyViewedPage(driver, wait, prop)
		new ContractsRecentlyViewedPage()
		.loadContractTable()
		.iterateAndSelectContract(contract_id)
		.clickDeleteContract();
	}
	
	@Then("contract is deleted")
	public void checkDeletedContract() throws InterruptedException {
		//new ContractsRecentlyViewedPage(driver, wait, prop)
		new ContractsRecentlyViewedPage()
		.validateToastMsg(dataMap.get("toast"));
	}
	
	@Then("User able to view and assert the deleted contract in recently_viewed item")
	public void validateDeletedContract() throws InterruptedException {
	//new ContractsRecentlyViewedPage(driver, wait, prop)
	new ContractsRecentlyViewedPage()
	.clickSearchContract()
	.enterContractAndSearch(contract_id)
	.loadContractTable()
	.validateEmptyTable(dataMap.get("emptyText"));
	}
	
}
