Feature: Create, Edit and Delete Tasks

@run1
Scenario: Tasks: TC-01_createTask
Given User clicks the applauncher
And selects the sales application
When User creates new task
Then new task is created
And User able to view and assert the new task in recently_viewed item

@run1
Scenario: Tasks: TC-02_editTask
Given User clicks the applauncher
And selects the sales application
And selects the recently viewed tasks
When User edits the task
Then User able to view and assert the edited task in recently_viewed item

@run1
Scenario: Tasks: TC-03_deleteTask
Given User clicks the applauncher
And selects the sales application
And selects the recently viewed tasks
When User deletes the task
Then User able to view and assert the task is deleted