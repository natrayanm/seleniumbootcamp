Feature: Create, Edit and Delete Contracts

@run1
Scenario: Contracts: TC-01_createContract
Given User clicks the applauncher
And selects the contracts
When User creates new contract
Then new contract is created
And User able to view and assert the new contract in recently_viewed item

@run1
Scenario: Contracts: TC-02_editContract
Given User clicks the applauncher
And selects the contracts
When User edits contract
Then contract is updated
And User able to view and assert the updated contract in recently_viewed item

@run1
Scenario: Contracts: TC-03_deleteContract
Given User clicks the applauncher
And selects the contracts
When User deletes contract
Then contract is deleted
And User able to view and assert the deleted contract in recently_viewed item