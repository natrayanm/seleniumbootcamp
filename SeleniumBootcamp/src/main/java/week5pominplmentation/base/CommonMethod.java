package week5pominplmentation.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestNGMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.github.bonigarcia.wdm.WebDriverManager;
import week3TestNg.ReadExcel;
import week5pominplmentation.pages.SalesForceLoginPage;

public class CommonMethod {
	
	//testng POM
	public RemoteWebDriver driver;
	public WebDriverWait wait;
	public String fileName;
	public String sheetName;
	public Properties prop;
	public static String contract_id;

	
	//Cucumber
	/*public static RemoteWebDriver driver;
	public static WebDriverWait wait;
	public static Properties prop;
	public String fileName;
	public String sheetName;
	public static ExtentReports extent;
	public static ExtentTest test;
	public static ExtentSparkReporter spark;
	//needed for cucmber. Not needed for testng POM
	public static String contract_id;
	public static Map<String, String> dataMap = new LinkedHashMap<String, String>(); */
	public static Map<String, String> dataMap = new LinkedHashMap<String, String>();
	
	@BeforeMethod(groups= {"Common"})
	@Parameters({ "url", "def_dwnld_dir" })
	public void setupBrowser(String url, String def_dwnld_dir) throws IOException {
		System.out.println("Calling @before");
		
		WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		
		HashMap<String, Object> prefs = new HashMap<>();
		prefs.put("plugins.always_open_pdf_externally", true);
		prefs.put("download.default_directory", def_dwnld_dir);
		options.setExperimentalOption("prefs", prefs);
		
		
		driver = new ChromeDriver(options);
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		wait=new WebDriverWait(driver, 30);
		
		FileInputStream fis = new FileInputStream("./src/main/java/week6/property/config.properties");
		prop = new Properties();
		prop.load(fis);
		
	}
	
	@AfterMethod(groups= {"Common"})
	public void closeBrowser() {
		driver.close();
	} 

	@DataProvider(name="FetchData")
	public String[][] sendData() throws IOException {
		return ReadExcel.readExcel(fileName, sheetName);
	}
	
	@DataProvider(name="FetchDataForEachTest")
	public String[][] sendDatanew(ITestNGMethod testContext) throws IOException {
		XSSFWorkbook wb = new XSSFWorkbook(fileName);
		XSSFSheet ws = wb.getSheet(testContext.getMethodName());
		
		if( ws == null) {
			return ReadExcelSheet.readExcel(fileName, sheetName);
		}
		else
			return ReadExcelSheet.readExcel(fileName, testContext.getMethodName());

		
	}
	
	public void login(String username, String password) {
	//	new SalesForceLoginPage(driver, wait, prop).enterUsername(username).enterPassword(password).clickLogin();
	}
		
	@SuppressWarnings("unchecked")
	public Map<String, String> sendDatanew(String scenario) throws IOException {
		String[] names = scenario.split(":");
		//String[] names = str.split(":");
		//System.out.println(names[0]);
		//System.out.println((names[1].split("_"))[1]);

		fileName = "C:\\Natrayan\\Testleaf\\Docs\\"+names[0]+".xlsx";
		sheetName= (names[1].split("_"))[1];


		@SuppressWarnings("resource")
		XSSFWorkbook  wb =  new XSSFWorkbook(fileName);
		XSSFSheet ws = wb.getSheet(sheetName);

		Row row = ws.getRow(0);
		int lastcolumnused = row.getLastCellNum();
		int colnum = 0;
		for (int i = 0; i < lastcolumnused; i++) {
			if (row.getCell(i).getStringCellValue().equalsIgnoreCase("TC-01")) {
				colnum = i;
				break;
			}
		}


		for(int i=1; i<= wb.getSheet(sheetName).getLastRowNum(); i++) {
			row = ws.getRow(i);
			Cell column = row.getCell(colnum);
			String cellValue = column.getStringCellValue();
			System.out.println(cellValue);

			ObjectMapper objectMapper = new ObjectMapper();
			try {    
				dataMap = objectMapper.readValue(cellValue, Map.class);   
			}catch (Exception e) { 
				e.printStackTrace();
			}
			System.out.println("Map is " + dataMap);
			System.out.println("Map Size is " + dataMap.size());
		}

		return dataMap;

	}
	
	/*public void setupExtent() throws FileNotFoundException, IOException {
	extent = new ExtentReports();
	spark = new ExtentSparkReporter("index.html");
	extent.attachReporter(spark);
	prop = new Properties();
	prop.load(new FileInputStream("./src/test/resources/extent.properties"));
	spark.loadXMLConfig("./src/test/resources/extent-config.xml");
	spark.config().setTheme(Theme.STANDARD);
	spark.config().setDocumentTitle("Automation Report");
	spark.config().setReportName("CRM 6.0");
	}
	
	
	public void setupTestExtent(String testcase) throws ClassNotFoundException {
		 test = extent.createTest(testcase);
		 test.assignAuthor("Natrayan");
		 test.assignCategory("Regression");
		 test.createNode("Step1");	
		 }
	
	public void flushReport() {
		extent.flush();
	} */

}
