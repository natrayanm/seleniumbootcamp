package week5pominplmentation.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DriverInstance {
	
	public String def_dwnld_dir = "C:\\Users\\natra\\Downloads\\selenium";

	private static final ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<RemoteWebDriver>();
	private static final ThreadLocal<WebDriverWait> wait = new ThreadLocal<WebDriverWait>();
	private static final ThreadLocal<Properties> prop = new ThreadLocal<Properties>();

	//@SuppressWarnings("deprecation")
	public void setWait() {
		wait.set(new WebDriverWait(getDriver(), 30));
	}

	public WebDriverWait getWait() {
		return wait.get();
	}
	
	public void setProp() {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("./src/main/java/week6/property/config.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		prop.set(new Properties());
		try {
			prop.get().load(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Properties getProp() {
		return prop.get();
	}

	public void setDriver(String browser, boolean headless) {
		switch(browser) {
		case "chrome" : {
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-notifications");

			HashMap<String, Object> prefs = new HashMap<>();
			prefs.put("plugins.always_open_pdf_externally", true);
			prefs.put("download.default_directory", def_dwnld_dir);
			options.setExperimentalOption("prefs", prefs);
			driver.set(new ChromeDriver(options));
			System.out.println("set Driver");
			}
		}
	}

	public RemoteWebDriver getDriver() {
		return driver.get();
	}
}
