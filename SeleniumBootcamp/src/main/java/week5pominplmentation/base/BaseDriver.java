package week5pominplmentation.base;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.opentelemetry.exporter.logging.SystemOutLogExporter;

public class BaseDriver extends DriverInstance{

	//public RemoteWebDriver driver;
	//public WebDriverWait wait;
	//public Properties prop;
	public String fileName;
	public String sheetName;
	public static String contract_id;


	public void startApp(String browser, String url, String def_dwnld_dir) {
		System.out.println("Calling @before");
		System.out.println("Browser in startupApp is "+browser);


		/*ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");

		HashMap<String, Object> prefs = new HashMap<>();
		prefs.put("plugins.always_open_pdf_externally", true);
		prefs.put("download.default_directory", def_dwnld_dir);
		options.setExperimentalOption("prefs", prefs);


		driver = new ChromeDriver(options);*/
		
		if(browser.equalsIgnoreCase("chrome")) {
			WebDriverManager.chromedriver().setup();
			System.out.println("inside startupAPP and set driver");
			setDriver(browser, false);
		}
		
		setWait();
		getDriver().get(url);
		getDriver().manage().window().maximize();
		getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		setProp();
		
		//getWait()=new WebDriverWait(driver, 30);

		
	/*	FileInputStream fis = null;
		try {
			fis = new FileInputStream("./src/main/java/week6/property/config.properties");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		prop = new Properties();
		try {
			prop.load(fis);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} */

	}
	
	public WebElement locateElement(String locator, String loccValue) {

		switch(locator.toLowerCase()) {
		case "id": return getDriver().findElement(By.id(loccValue));
		case "name": return getDriver().findElement(By.name(loccValue));
		case "class": return getDriver().findElement(By.className(loccValue));
		case "link": return getDriver().findElement(By.linkText(loccValue));
		case "tag": return getDriver().findElement(By.tagName(loccValue));
		case "xpath": return getDriver().findElement(By.xpath(loccValue));
		default: return null;

		}

	}

	public List<WebElement> locateElements(String locator, String loccValue) {

		switch(locator.toLowerCase()) {
		case "id": return getDriver().findElements(By.id(loccValue));
		case "name": return getDriver().findElements(By.name(loccValue));
		case "class": return getDriver().findElements(By.className(loccValue));
		case "link": return getDriver().findElements(By.linkText(loccValue));
		case "tag": return getDriver().findElements(By.tagName(loccValue));
		case "xpath": return getDriver().findElements(By.xpath(loccValue));
		default: return null;

		}

	}

	public void type(WebElement ele, String data) {
		ele.clear();
		ele.sendKeys(data);
	}

	public void click(WebElement ele) {
		ele.click();
	}
	
	public void clearText(WebElement ele) {
		ele.clear();
	}

	public void executeScript(WebElement ele, String arg) {
		getDriver().executeScript(arg, ele);

	}

	public void executeJavaScript(WebElement ele, String arg) {
		JavascriptExecutor executor = (JavascriptExecutor)getDriver();
		executor.executeScript(arg, ele); 
	}

	public void performAction(String action, int action_Cnt) {
		//String action_Key="";
		 Actions keyDown = new Actions(getDriver());
		// action_Key = action_Key.substring(0, action_Key.length()-1);
		// System.out.println("action_Key is "+ action_Key);
		 switch(action) {
		 case "TAB": { 
			 for(int i=0; i<action_Cnt; i++) {
				 keyDown.sendKeys(Keys.TAB).build().perform(); 
			 }
			// keyDown.sendKeys(action_Key).build().perform(); 
			 }
		 
		 }
		   
	}
	
	public void explictWait(By locator, String condition) {

		switch(condition){
		case "visibilityOfElement": getWait().until(ExpectedConditions.visibilityOfElementLocated(locator));
		}

	}

	public String getText(WebElement ele) {
		return ele.getText();

	}

	public String getTitle() {
		return getDriver().getTitle();

	}


}
