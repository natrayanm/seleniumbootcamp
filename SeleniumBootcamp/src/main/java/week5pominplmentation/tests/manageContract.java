package week5pominplmentation.tests;

import java.text.ParseException;

import org.openqa.selenium.By;
import org.testng.ISuite;
import org.testng.ITestContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import week5pomImplementation.hooks.TestNgHooks;
import week5pominplmentation.pages.SalesForceLandingPage;
import week5pominplmentation.pages.SalesForceLoginPage;

public class manageContract extends TestNgHooks{
	
	//Should be commented for testng
	//public String contract_id;

	
	@BeforeTest(groups = {"Common"})
	public void setFilename() {
		System.out.println("Calling Before test");
		this.fileName = "C:\\Natrayan\\Testleaf\\Docs\\ReadExcel.xlsx";
		//this.sheetName="Sheet4";
		System.out.println("Filename in setfilename is "+fileName);
	}
	
	@Parameters({  "username", "password" })
	@BeforeMethod(groups = {"Common"})
	public void login(String username, String password) {
	//	new SalesForceLoginPage(getDriver(), getWait(), prop).enterUsername(username).enterPassword(password).clickLogin();
		new SalesForceLoginPage().enterUsername(username).enterPassword(password).clickLogin();
		
	}
	
	@Test(groups= {"Contracts"}, dataProvider="FetchDataForEachTest")
	public void createContracts(ITestContext context, String accName, String sDate, String period, String toast) throws InterruptedException, ParseException {
		
		System.out.println("Title while landing "+ getDriver().getTitle());
		if(getDriver().getTitle().equals("Home Page ~ Salesforce - Developer Edition")) {
			getDriver().findElement(By.xpath("//div/div/a[@class=\"switch-to-lightning\"]")).click();
			System.out.println("Title after switching "+getDriver().getTitle());
		}
		
//		new SalesForceLandingPage(getDriver(), getWait(), prop)
		new SalesForceLandingPage()
		.clickApplauncher()
		.clickviewAll()
		.scrollAndClickContracts()
		.clickNewContract()
		.clickSearchAccounts()
		.clickOrCreateAccount(accName)
		.enterContractStartDate(sDate)
		.enterContractPeriod(period)
		.saveContract()
		.validateToastMsg(toast)
		.extractContractId(context)
		.clickNewlyCreatedContract()
		.loadContractTable()
		.iterateContractTable(accName);
		
		ISuite suite = context.getSuite();
		contract_id = (String)suite.getAttribute("contract_id");

		
	}
	
	@Test(groups= {"Contracts"}, dataProvider="FetchDataForEachTest", dependsOnMethods = "createContracts")
	public void editContract(ITestContext context, String accName, String status, String cEDate, String toast) throws InterruptedException, ParseException {
		
		System.out.println("Contract in edit test case is "+contract_id);
		System.out.println("Title while landing "+ getDriver().getTitle());
		if(getDriver().getTitle().equals("Home Page ~ Salesforce - Developer Edition")) {
			getDriver().findElement(By.xpath("//div/div/a[@class=\"switch-to-lightning\"]")).click();
			System.out.println("Title after switching "+getDriver().getTitle());
		}
		
	//	new SalesForceLandingPage(getDriver(), getWait(), prop)
		new SalesForceLandingPage()
		.clickApplauncher()
		.clickviewAll()
		.scrollAndClickContracts()
		.loadContractTable()
		.iterateAndSelectContract(contract_id)
		.clickEditContract()
		.changeContractStatus(status)
		.changeExpirationDate(cEDate)
		.saveContract()
		.validateToastMsg(toast)
		.clickSearchContract()
		.enterContractAndSearch(contract_id)
		.loadContractTable()
		.iterateAndCompareContract(contract_id);
		
		Thread.sleep(5000);
		
	}

	@Test(groups= {"Contracts"}, dataProvider="FetchDataForEachTest", dependsOnMethods = "editContract")
	public void deleteContract(ITestContext context, String emptyText, String toast) throws InterruptedException, ParseException {
		
		System.out.println("Contract in delete test case is "+contract_id);
		System.out.println("Title while landing "+ getDriver().getTitle());
		if(getDriver().getTitle().equals("Home Page ~ Salesforce - Developer Edition")) {
			getDriver().findElement(By.xpath("//div/div/a[@class=\"switch-to-lightning\"]")).click();
			System.out.println("Title after switching "+getDriver().getTitle());
		}
		
		//new SalesForceLandingPage(getDriver(), getWait(), prop)
		new SalesForceLandingPage()
		.clickApplauncher()
		.clickviewAll()
		.scrollAndClickContracts()
		.loadContractTable()
		.iterateAndSelectContract(contract_id)
		.clickDeleteContract()
		.validateToastMsg(toast)
		.clickSearchContract()
		.enterContractAndSearch(contract_id)
		.loadContractTable()
		.validateEmptyTable(emptyText);
		
		Thread.sleep(5000);
		
	}

}
