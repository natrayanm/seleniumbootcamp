package week5pominplmentation.tests;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import week5pomImplementation.hooks.TestNgHooks;
import week5pominplmentation.base.BaseDriver;
import week5pominplmentation.base.CommonMethod;
import week5pominplmentation.pages.SalesForceHomePage;
import week5pominplmentation.pages.SalesForceLandingPage;
import week5pominplmentation.pages.SalesForceLoginPage;

public class editTask extends TestNgHooks{
	
		
		@BeforeTest(groups = {"Tasks"})
		public void setFilename() {
			System.out.println("Calling Before test");
			this.fileName = "C:\\Natrayan\\Testleaf\\Docs\\ReadExcel.xlsx";
			this.sheetName="Sheet2";
			System.out.println("Filename in setfilename is "+ fileName + sheetName);
		}
		
		@Parameters({  "username", "password" })
		@BeforeMethod(groups = {"Common"})
		public void login(String username, String password) {
			//new SalesForceLoginPage(driver, wait, prop).enterUsername(username).enterPassword(password).clickLogin();
			new SalesForceLoginPage().enterUsername(username).enterPassword(password).clickLogin();
			
		}
		
		@Test(groups= {"Tasks"}, dataProvider="FetchData")
		public void editTasks(String taskName, String toast, String dueDate, String priority) throws InterruptedException {
		
			System.out.println("Title while landing "+ getDriver().getTitle());
			if(getDriver().getTitle().equals("Home Page ~ Salesforce - Developer Edition")) {
				getDriver().findElement(By.xpath("//div/div/a[@class=\"switch-to-lightning\"]")).click();
				System.out.println("Title after switching "+getDriver().getTitle());
			}
			
			//new SalesForceLandingPage(driver, wait, prop)
			new SalesForceLandingPage()
			.clickApplauncher()
			.clickviewAll()
			.enterAndClickSales()
			.clickTasks()
			.clickRecentlyViewedDropdown()
			.selectRecentlyViewed()
			.clickTaskWithName(taskName)
			.clickEditDueDate()
			.enterDueDate(dueDate)
			.clickEditPriority()
			.selectPriority(priority)
			.clickSaveTask();
			
			//new SalesForceHomePage(driver, wait, prop)
			new SalesForceHomePage()
			.clickTasks()
			.clickRecentlyViewedDropdown()
			.selectRecentlyViewed()
			.clickTaskWithName(taskName)
			.assertTaskPriority(priority);
		}


}
