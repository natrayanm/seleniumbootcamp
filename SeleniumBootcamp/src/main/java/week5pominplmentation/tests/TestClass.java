package week5pominplmentation.tests;

import java.text.ParseException;

import org.testng.ITestContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import week5pominplmentation.base.CommonMethod;

public class TestClass extends CommonMethod{

	@BeforeTest(groups = {"common"})
	public void setFilename() {
		System.out.println("Calling Before test");
		this.fileName = "C:\\Natrayan\\Testleaf\\Docs\\ReadExcel.xlsx";
		this.sheetName="Sheet4";
		System.out.println("Filename in setfilename is "+fileName);
	}
	
	@Test(groups= {"common"}, dataProvider="FetchDataForEachTest")
	public void test1(String accName, String sDate, String period, String toast) {
		System.out.println(accName + sDate + period + toast );
	}
	
	@Test(groups= {"common"}, dataProvider="FetchDataForEachTest")
	public void test4(String accName, String sDate, String period, String toast) {
		System.out.println(accName + sDate + period + toast );
	}
}
