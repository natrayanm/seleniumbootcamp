package week5pominplmentation.utils;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryFailedTestCase implements IRetryAnalyzer{
	
	int max_count=1;
	int retry_count = 0;
	
	@Override
	public boolean retry(ITestResult result) {
		if(!result.isSuccess() && retry_count < max_count) {
			retry_count++;
			return true;
		}
		return false;
	}

}
