package week5pominplmentation.pages;

import org.openqa.selenium.By;
import org.testng.Assert;

import week5pomImplementation.hooks.TestNgHooks;

public class TasksHomePage extends TestNgHooks{
	
	/*public TasksHomePage(RemoteWebDriver driver, WebDriverWait wait, Properties prop) {
		this.driver=driver;
		this.wait=wait;
		this.prop=prop;
	}*/
	
	public TasksHomePage clickRecentlyViewedDropdown() throws InterruptedException {
		//driver.findElement(By.xpath(prop.getProperty("TasksHomePage.recentlyViewedDropdown.xpath"))).click();
		
		click(locateElement("xpath", getProp().getProperty("TasksHomePage.recentlyViewedDropdown.xpath")));
	    Thread.sleep(3000);
	    return this;
	}
	
	public TasksHomePage selectRecentlyViewed() throws InterruptedException {
		//driver.findElement(By.xpath(prop.getProperty("TasksHomePage.selectRecentlyViewed.xpath"))).click();
	    
		click(locateElement("xpath", getProp().getProperty("TasksHomePage.selectRecentlyViewed.xpath")));
		Thread.sleep(3000);
	    return this;
	   
	}
	
	public TasksHomePage clickTaskWithName(String taskName) {
		//WebElement element = driver.findElement(By.xpath(prop.getProperty("TasksHomePage.taskNameLink.xpath")+taskName+"']"));
	 	//driver.executeScript("arguments[0].click();", element);
	 	
	 	executeScript(locateElement("xpath", getProp().getProperty("TasksHomePage.taskNameLink.xpath")+taskName+"']"), "arguments[0].click();");
	 	return this;
	}
	
	public TasksHomePage assertTaskSubject(String taskName) {
		//String e_source = driver.findElement(By.xpath(prop.getProperty("TasksHomePage.taskSubject.xpath"))).getText();
	    
		String e_source = getText(locateElement("xpath", getProp().getProperty("TasksHomePage.taskSubject.xpath")));
		Assert.assertEquals(e_source,taskName);
	    return this;
	}
	
	public TasksHomePage assertTaskName(String name) {
		//String e_name = driver.findElement(By.xpath(prop.getProperty("TasksHomePage.taskName.xpath"))).getText();
		
		String e_name = getText(locateElement("xpath", getProp().getProperty("TasksHomePage.taskName.xpath")));
	 	Assert.assertEquals(e_name, name);
	 	return this;
	}
	
	public TasksHomePage clickEditDueDate() {
		//driver.findElement(By.xpath(prop.getProperty("TasksHomePage.editDueDate.xpath"))).click();
		
		click(locateElement("xpath", getProp().getProperty("TasksHomePage.editDueDate.xpath")));
		return this;
	}
	
	public TasksHomePage enterDueDate(String dueDate) throws InterruptedException {
		/*driver.findElement(By.xpath(prop.getProperty("TasksHomePage.enterDueDate.xpath"))).clear();
	    //Enter the date as string
	    driver.findElement(By.xpath(prop.getProperty("TasksHomePage.enterDueDate.xpath"))).sendKeys(dueDate);
	    //Come out of date field
	    Actions keyDown = new Actions(driver);
	    Thread.sleep(5000);
	    //this worked
	    keyDown.sendKeys(Keys.TAB, Keys.TAB).build().perform(); 
	    Thread.sleep(5000);*/
	    
	    
	    type(locateElement("xpath", getProp().getProperty("TasksHomePage.enterDueDate.xpath")), dueDate);
	    Thread.sleep(5000);
		performAction("TAB", 2);
		Thread.sleep(5000);
	    return this;
	}
	
	public TasksHomePage clickEditPriority() throws InterruptedException {
		// WebElement element = driver.findElement(By.xpath(prop.getProperty("TasksHomePage.editPriotity.xpath")));
		// driver.executeScript("arguments[0].click();", element);
		 
		 executeJavaScript(locateElement("xpath", getProp().getProperty("TasksHomePage.editPriotity.xpath")), "arguments[0].click();");
		 Thread.sleep(5000);
		 return this;
		   
	}
	
	public TasksHomePage selectPriority(String priority) {
		//driver.findElement(By.xpath(prop.getProperty("TasksHomePage.selectPriority.xpath")+priority+"\"]")).click();
		
		click(locateElement("xpath", getProp().getProperty("TasksHomePage.selectPriority.xpath")+priority+"\"]"));
		return this;	
	}
	
	public TasksHomePage clickSaveTask() throws InterruptedException {
		//driver.findElement(By.xpath(prop.getProperty("TasksHomePage.saveTask.xpath"))).click();
		
		click(locateElement("xpath", getProp().getProperty("TasksHomePage.saveTask.xpath")));
	    Thread.sleep(5000);
	    return this;
	}
	
	public TasksHomePage assertTaskPriority(String priority) {
		 //String e_priority = driver.findElement(By.xpath(prop.getProperty("TasksHomePage.taskPriority.xpath"))).getText();
		 
		 String e_priority = getText(locateElement("xpath", getProp().getProperty("TasksHomePage.taskPriority.xpath")));
		 Assert.assertEquals(e_priority, priority);
		 return this;
		   
	}
	
	public TasksHomePage clickMoreActionForTask() throws InterruptedException {
		//Expand the arrow for more actions
		Thread.sleep(1000);
	    
		/*WebElement element = driver.findElement(By.xpath(prop.getProperty("TasksHomePage.moreAction.xpath")));
	    JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);*/
	    
	    executeJavaScript(locateElement("xpath", getProp().getProperty("TasksHomePage.moreAction.xpath")), "arguments[0].click();");
	    return this;
	}
	
	public TasksHomePage clickDeleteTask() throws InterruptedException {

		//driver.findElement(By.xpath(prop.getProperty("TasksHomePage.deleteTask.xpath"))).click();
		//driver.findElement(By.xpath(prop.getProperty("TasksHomePage.deleteTaskButton.xpath"))).click();
		
		click(locateElement("xpath", getProp().getProperty("TasksHomePage.deleteTask.xpath")));
		click(locateElement("xpath", getProp().getProperty("TasksHomePage.deleteTaskButton.xpath")));
		return this;
	}
	
	public void validateToastMsg(String message) {
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("TasksHomePage.toast.xpath"))));
	    //String contact_toast = driver.findElement(By.xpath(prop.getProperty("TasksHomePage.toast.xpath"))).getText();
	    
	    explictWait(By.xpath(getProp().getProperty("TasksHomePage.toast.xpath")), "visibilityOfElement");
	    String contact_toast = getText(locateElement("xpath", getProp().getProperty("TasksHomePage.toast.xpath"))) ;

	    if (contact_toast.contains(message)) {
	    	System.out.println("task is deleted");
	    }
	}
}
