package week5pominplmentation.pages;

import org.openqa.selenium.By;

import week5pomImplementation.hooks.TestNgHooks;

public class SalesForceHomePage extends TestNgHooks{
	
	/*public SalesForceHomePage(RemoteWebDriver driver, WebDriverWait wait, Properties prop) {
		this.driver=driver;
		this.wait=wait;
		this.prop=prop;
	}*/
	
	public SalesForceHomePage expandTask() {
		//driver.findElement(By.xpath(prop.getProperty("SalesForceHomePage.expandTask.xpath"))).click();
		
		click(locateElement("xpath", getProp().getProperty("SalesForceHomePage.expandTask.xpath")));
		return this;
	}
	
	public SalesForceHomePage clickNewTask() {
		/*WebElement element = driver.findElement(By.xpath(prop.getProperty("SalesForceHomePage.newTask.xpath")));
		//JavascriptExecutor executor = (JavascriptExecutor)driver;
	    //executor.executeScript("arguments[0].click();", element);
	    driver.executeScript("arguments[0].click();", element);*/
		
	    executeScript(locateElement("xpath", getProp().getProperty("SalesForceHomePage.newTask.xpath")), "arguments[0].click()");
	    return this;
	}
	
	public SalesForceHomePage enterTaskName(String taskName) {
	   // driver.findElement(By.xpath(prop.getProperty("SalesForceHomePage.taskName.xpath"))).sendKeys(taskName);
		
		type(locateElement("xpath", getProp().getProperty("SalesForceHomePage.taskName.xpath")), taskName);
	    return this;

	}
	
	public SalesForceHomePage selectStatus(String status ) {
		/*driver.findElement(By.xpath(prop.getProperty("SalesForceHomePage.selectStatus.xpath"))).click();
	    WebElement element = driver.findElement(By.xpath("//div/ul//li//a[text()=\""+status+"\"]"));
		//executor = (JavascriptExecutor)driver;
	    //executor.executeScript("arguments[0].click();", element);
	    driver.executeScript("arguments[0].click();", element);*/
		
		click(locateElement("xpath", getProp().getProperty("SalesForceHomePage.selectStatus.xpath")));
		executeScript(locateElement("xpath", "//div/ul//li//a[text()=\""+status+"\"]"), "arguments[0].click()");
	    return this;

	}
	
	public SalesForceHomePage clickSearchContacts() {
		// driver.findElement(By.xpath(prop.getProperty("SalesForceHomePage.serachContracts.xpath"))).click();
		
		click(locateElement("xpath", getProp().getProperty("SalesForceHomePage.serachContracts.xpath")));
		 return this;
	   
	}
	
	public SalesForceHomePage createNewContact(String name, String message) throws InterruptedException {
		/* driver.findElement(By.xpath(prop.getProperty("SalesForceHomePage.newContact.xpath"))).click();
		    Thread.sleep(2000);
		    driver.findElement(By.xpath(prop.getProperty("SalesForceHomePage.contactInformation.xpath"))).click();
		    driver.findElement(By.xpath(prop.getProperty("SalesForceHomePage.nameSalutation.xpath"))).click();
		    driver.findElement(By.xpath(prop.getProperty("SalesForceHomePage.salutationMr.xpath"))).click();
		    //driver.findElement(By.xpath("//input[@placeholder=\"First Name\"]")).sendKeys("Natrayan");
		    driver.findElement(By.xpath(prop.getProperty("SalesForceHomePage.lastName.xpath"))).sendKeys(name);
		    driver.findElement(By.xpath(prop.getProperty("SalesForceHomePage.saveContact.xpath"))).click();
		    Thread.sleep(2000);
		    validateToastMsg(message);*/
		
		click(locateElement("xpath", getProp().getProperty("SalesForceHomePage.newContact.xpath")));
		Thread.sleep(2000);
	    click(locateElement("xpath", getProp().getProperty("SalesForceHomePage.contactInformation.xpath")));
		click(locateElement("xpath", getProp().getProperty("SalesForceHomePage.nameSalutation.xpath")));
		click(locateElement("xpath", getProp().getProperty("SalesForceHomePage.salutationMr.xpath")));
		type(locateElement("xpath", getProp().getProperty("SalesForceHomePage.lastName.xpath")), name);
		click(locateElement("xpath", getProp().getProperty("SalesForceHomePage.saveContact.xpath")));
		Thread.sleep(2000);
	    validateToastMsg(message);
		return this;
			
	}
	
	public void validateToastMsg(String message) {
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("SalesForceHomePage.toast.xpath"))));
		//String contact_toast = driver.findElement(By.xpath(prop.getProperty("SalesForceHomePage.toast.xpath"))).getText();
	
		explictWait(By.xpath(getProp().getProperty("SalesForceHomePage.toast.xpath")), "visibilityOfElement");
		String contact_toast = getText(locateElement("xpath", getProp().getProperty("SalesForceHomePage.toast.xpath")));
				
	    if (contact_toast.contains(message)) {
	    	System.out.println("contact is created");
	    }
	}

	public SalesForceHomePage saveNewTask(String message) throws InterruptedException {
		Thread.sleep(5000);
	    //driver.findElement(By.xpath(prop.getProperty("SalesForceHomePage.saveTask.xpath"))).click();
		//System.out.println(driver.getTitle());
	    
	    click(locateElement("xpath", getProp().getProperty("SalesForceHomePage.saveTask.xpath")));
	    System.out.println(getTitle());
	    
	    validateToastMsg(message);
		return this;
	}
	
	public TasksHomePage clickTasks() throws InterruptedException {
		// WebElement element = driver.findElement(By.xpath(prop.getProperty("SalesForceHomePage.taskTab.xpath")));
		 //driver.executeScript("arguments[0].click();", element);
		 
		 executeScript(locateElement("xpath", getProp().getProperty("SalesForceHomePage.taskTab.xpath")), "arguments[0].click();");
		 Thread.sleep(3000);
		 //return new TasksHomePage(driver, wait, prop);
		 return new TasksHomePage();

	}
}
