package week5pominplmentation.pages;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.ISuite;
import org.testng.ITestContext;

import week5pomImplementation.hooks.TestNgHooks;

public class ContractsRecentlyViewedPage extends TestNgHooks{
	
	/*public ContractsRecentlyViewedPage(RemoteWebDriver driver, WebDriverWait wait, Properties prop) {
		this.driver=driver;
		this.wait=wait;
		this.prop=prop;
	}*/
	
	//Commented for cucumber. Needed for testnG POM
	//public static String contract_id;
	public static String contract_toast;
	
	public ContractsRecentlyViewedPage clickNewContract() throws InterruptedException {
		Thread.sleep(3000);
		//driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.newContract.xpath"))).click();
		
		click(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.newContract.xpath")));
		return this;
	}
	
	public ContractsRecentlyViewedPage clickSearchAccounts() {
		//driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.searchAccount.xpath"))).click();
		
		click(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.searchAccount.xpath")));
		return this;
	}
	
	public ContractsRecentlyViewedPage clickOrCreateAccount(String name) throws InterruptedException {
		try {
			//driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.selectAccount.xpath")+name+"']")).click();
			
			click(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.selectAccount.xpath")+name+"']"));


		}
		//catch(NoSuchElementException e) {
		catch(Exception e) {
			System.out.println("Exception is caught");
			/*driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.newAccount.xpath"))).click();
			driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.accountInformation.xpath"))).click();
			Thread.sleep(5000);
			driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.accountName.xpath"))).sendKeys(name);
			driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.saveAccount.xpath"))).click(); */
			
			click(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.newAccount.xpath")));
			click(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.accountInformation.xpath")));
			Thread.sleep(5000);
			type(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.accountName.xpath")), name);
			click(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.saveAccount.xpath")));


		}
		return this;

	}
	
	public ContractsRecentlyViewedPage enterContractStartDate(String sdate) throws InterruptedException {
		Thread.sleep(4000);
		//Enter the contract start date
	/*	driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.contractStartDate.xpath"))).clear();
		driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.contractStartDate.xpath"))).sendKeys(sdate);
		Actions keyDown = new Actions(driver);
	    Thread.sleep(5000);
	    //this worked
	    keyDown.sendKeys(Keys.TAB, Keys.TAB).build().perform();*/
	    
	    click(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.contractStartDate.xpath")));
		type(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.contractStartDate.xpath")), sdate);
	    Thread.sleep(5000);
		performAction("TAB", 2);
		return this;
	}
	
	public ContractsRecentlyViewedPage enterContractPeriod(String period) {
		//driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.contractPeriod.xpath"))).sendKeys(period);
		
		type(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.contractPeriod.xpath")), period);
		return this;
	}
	
	public ContractsRecentlyViewedPage saveContract() {
		//driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.saveContract.xpath"))).click();
		
	    click(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.saveContract.xpath")));

		return this;
	}
	
	public ContractsRecentlyViewedPage validateToastMsg(String message) throws InterruptedException {
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.toast.xpath"))));
	    //contract_toast = driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.toast.xpath"))).getText();
	    
	    explictWait(By.xpath(getProp().getProperty("ContractsRecentlyViewedPage.toast.xpath")), "visibilityOfElement");
	    contract_toast = getText(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.toast.xpath")));
	    if (contract_toast.contains(message)) {
	    	System.out.println("contact is created");
	    	//extractContractId(contract_toast);
	    }
		return this;

	}
	    
	public ContractsRecentlyViewedPage extractContractId(ITestContext context) throws InterruptedException {
		String contract_patern = "([0-9]+)";
	    
	    Pattern pattern = Pattern.compile(contract_patern);
	    Matcher matcher = pattern.matcher(contract_toast);
	    boolean matchFound = matcher.find();
	    contract_id=matcher.group();
	    if(matchFound) {
	      System.out.println("Match found " + contract_id);
	    } else {
	      System.out.println("Match not found");
	    }
	    Thread.sleep(5000);
		//System.out.println("//a[@title=\"" + contract_id + "\"]/span[text()=\"" + contract_id + "\"]");
		//commented for cucumber. Required for POM testng
	    if(context!=null) {
	    	ISuite suite = context.getSuite();
			suite.setAttribute("contract_id", contract_id); 
	    }
	   return this;

	}
	
	public ContractsRecentlyViewedPage clickNewlyCreatedContract() {
		/*WebElement element = driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.contractTab[1].xapth") + contract_id + prop.getProperty("ContractsRecentlyViewedPage.contractTab[2].xapth") + contract_id + "\"]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);*/
	    
	    executeJavaScript(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.contractTab[1].xapth") + contract_id + getProp().getProperty("ContractsRecentlyViewedPage.contractTab[2].xapth") + contract_id + "\"]"), "arguments[0].click();");
	    return this;
	}
	
	public ContractsRecentlyViewedPage closeNewContractTab() {
		/*WebElement element = driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.contractTab[3].xapth") + " "+ contract_id +  "\"]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);*/
	    
	    executeJavaScript(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.contractTab[3].xapth") + " "+ contract_id +  "\"]"), "arguments[0].click();");
	    return this;
	}
	
	//button[@title="Close 00000237"]
	
	public ContractsRecentlyViewedPage loadContractTable() throws InterruptedException {
		//wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.recentlyViewedContractTable.xpath"))));
		//driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.recentlyViewedContractTable.xpath")));
		
		explictWait(By.xpath(getProp().getProperty("ContractsRecentlyViewedPage.recentlyViewedContractTable.xpath")), "presenceOfElement");
		Thread.sleep(2000);
		return this;
	}
	
	public ContractsRecentlyViewedPage iterateContractTable(String name) throws ParseException {
		//List<WebElement> elements_row = driver.findElements(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.recentlyViewedContractTable.xpath")));
		
		List<WebElement> elements_row = locateElements("xpath", getProp().getProperty("ContractsRecentlyViewedPage.recentlyViewedContractTable.xpath"));
		System.out.println("Rows in table are " + elements_row.size());
		
		//iterate rows and find the name, contract_id, start_date and end_date for each contract/row
		for (int i=1; i<=elements_row.size(); i++) {
			/*
			//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).getText());
			String contract_id_tb = driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.contractTableRow[1].xpath")+ i +prop.getProperty("ContractsRecentlyViewedPage.contractTableRow[2].xpath"))).getText();
			//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[3]/span/a")).getText());
			String contract_name = driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.contractTableRow[1].xpath")+i+prop.getProperty("ContractsRecentlyViewedPage.contractTableRow[3].xpath"))).getText();
			//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[5]/span")).getText());
			String contract_st_date = driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.contractTableRow[1].xpath")+i+prop.getProperty("ContractsRecentlyViewedPage.contractTableRow[4].xpath"))).getText();
			//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[6]/span")).getText());
			String contract_end_date=driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.contractTableRow[1].xpath")+i+prop.getProperty("ContractsRecentlyViewedPage.contractTableRow[5].xpath"))).getText();
			System.out.println("Contract Name is "+contract_name+ " Contract Id is "+contract_id_tb+" contract start date is "+ contract_st_date + " contract end date is "+contract_end_date);
			*/
			
			String contract_id_tb = getText(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.contractTableRow[1].xpath")+ i +getProp().getProperty("ContractsRecentlyViewedPage.contractTableRow[2].xpath")));
			String contract_name = getText(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.contractTableRow[1].xpath")+i+getProp().getProperty("ContractsRecentlyViewedPage.contractTableRow[3].xpath")));
			String contract_st_date = getText(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.contractTableRow[1].xpath")+i+getProp().getProperty("ContractsRecentlyViewedPage.contractTableRow[4].xpath")));
			String contract_end_date = getText(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.contractTableRow[1].xpath")+i+getProp().getProperty("ContractsRecentlyViewedPage.contractTableRow[5].xpath")));
			System.out.println("Contract Name is "+contract_name+ " Contract Id is "+contract_id_tb+" contract start date is "+ contract_st_date + " contract end date is "+contract_end_date);
			
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			
			Date d1 = sdf.parse(contract_st_date);
	        Date d2 = sdf.parse(contract_end_date);
	        long difference_In_Time = d2.getTime() - d1.getTime();
	        
	        long difference_In_Months = TimeUnit.MILLISECONDS.toDays(difference_In_Time)/30l;
	        
	        System.out.println("Difference in months is "+ difference_In_Months);
	        
			if (contract_name.equals(name)) {
				System.out.println("contract is created");
			}
		}
		return this;
	}
	
	public ContractsRecentlyViewedPage iterateAndSelectContract(String contract_id) throws InterruptedException {
		
		//ISuite suite = context.getSuite();
		//String contract_id = (String)suite.getAttribute("contract_id");
		
		//List<WebElement> elements_row = driver.findElements(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.contractTableRow[6].xpath")));
		List<WebElement> elements_row = locateElements("xpath", getProp().getProperty("ContractsRecentlyViewedPage.contractTableRow[6].xpath"));
		
		System.out.println("Rows in table are " + elements_row.size());
		System.out.println("contract_id in edit is "+contract_id);
		//iterate rows and find the name, contract_id, start_date and end_date for each contract/row
		for (int i=1; i<=elements_row.size(); i++) {
			//String contract_id_tb = driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.contractTableRow[1].xpath")+ i +prop.getProperty("ContractsRecentlyViewedPage.contractTableRow[2].xpath"))).getText();
			String contract_id_tb = getText(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.contractTableRow[1].xpath")+ i +getProp().getProperty("ContractsRecentlyViewedPage.contractTableRow[2].xpath")));

			if (contract_id_tb.equals(contract_id)) {
				System.out.println("contract inside edit is "+contract_id);
				/*WebElement element = driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.contractShowAction[1].xpath") +contract_id + prop.getProperty("ContractsRecentlyViewedPage.contractShowAction[2].xpath")));
				JavascriptExecutor executor = (JavascriptExecutor)driver;
			    executor.executeScript("arguments[0].click();", element);*/
			    
			    executeJavaScript(locateElement("xpath",getProp().getProperty("ContractsRecentlyViewedPage.contractShowAction[1].xpath") +contract_id + getProp().getProperty("ContractsRecentlyViewedPage.contractShowAction[2].xpath")), "arguments[0].click();");
			    
			    // Click Edit option
			    Thread.sleep(2000);
			}
		}
		return this;

	}
	
	public ContractsRecentlyViewedPage clickEditContract() {
		// driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.editContract.xpath"))).click();
		 
		 click(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.editContract.xpath")));
		   return this;
	}
	
	public ContractsRecentlyViewedPage clickDeleteContract() {
	   /* driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.deleteContract.xpath"))).click();
		WebElement element = driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.deleteContractButton.xpath")));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);*/
	    
		click(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.deleteContract.xpath")));
	    executeJavaScript(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.deleteContractButton.xpath")), "arguments[0].click();");
        return this;
	}
	
	public ContractsRecentlyViewedPage changeContractStatus(String status) throws InterruptedException {
		/*driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.contractStatus.xpath"))).click();
		//Select status as Activated
		driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.changeContractStatus.xpath")+status+"']")).click();*/
		
		click(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.contractStatus.xpath")));
		click(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.changeContractStatus.xpath")+status+"']"));

		Thread.sleep(1000);
		return this;
	}
	
	public ContractsRecentlyViewedPage changeExpirationDate(String period) {
		/*driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.contractExpirationDate.xpath"))).click();
		//select 30 days
		driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.enterExpirationDate.xpath")+period+"']")).click();
		*/
		
		click(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.contractExpirationDate.xpath")));
		click(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.enterExpirationDate.xpath")+period+"']"));

		return this;
	}
	
	public ContractsRecentlyViewedPage clickSearchContract() {
		//driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.searchContract.xpath"))).clear();
		
		clearText(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.searchContract.xpath")));
		return this;
	}
	
	public ContractsRecentlyViewedPage enterContractAndSearch(String contract_id) {
		/*driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.searchContract.xpath"))).sendKeys(contract_id);
		Actions keyDown = new Actions(driver);
		keyDown.sendKeys(Keys.ENTER).build().perform();*/
		
		type(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.searchContract.xpath")), contract_id);
		performAction("ENTER", 1);
		return this;
	}
	
	public ContractsRecentlyViewedPage iterateAndCompareContract(String contract_id) throws InterruptedException {
		
		//ISuite suite = context.getSuite();
		//String contract_id = (String)suite.getAttribute("contract_id");
		
		//List<WebElement> elements_row = driver.findElements(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.contractTableRow[6].xpath")));
		
		List<WebElement> elements_row = locateElements("xpath",getProp().getProperty("ContractsRecentlyViewedPage.contractTableRow[6].xpath"));
		System.out.println("Rows in table are " + elements_row.size());
		System.out.println("contract_id in edit is "+contract_id);
		//iterate rows and find the name, contract_id, start_date and end_date for each contract/row
		for (int i=1; i<=elements_row.size(); i++) {
			//String contract_id_tb = driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.contractTableRow[1].xpath")+ i +prop.getProperty("ContractsRecentlyViewedPage.contractTableRow[2].xpath"))).getText();
			
			String contract_id_tb = getText(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.contractTableRow[1].xpath")+ i +getProp().getProperty("ContractsRecentlyViewedPage.contractTableRow[2].xpath")));
			if (contract_id_tb.equals(contract_id)) {
				System.out.println("contract id matching after edit "+contract_id);
				Thread.sleep(1000);
			}
		}
		return this;

	}

	public ContractsRecentlyViewedPage validateEmptyTable(String emptytext) {
		//String text = driver.findElement(By.xpath(prop.getProperty("ContractsRecentlyViewedPage.emptyTable.xpath"))).getText();
		
		String text = getText(locateElement("xpath", getProp().getProperty("ContractsRecentlyViewedPage.emptyTable.xpath")));
		System.out.println("Result text of search after deletion is " + text);
		
		
		if (text.equals(emptytext)){
			System.out.println("Contract deletion successful");
		}
		return this;


	}

}
