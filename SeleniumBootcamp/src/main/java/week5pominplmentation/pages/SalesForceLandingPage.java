package week5pominplmentation.pages;

import org.openqa.selenium.By;

import week5pomImplementation.hooks.TestNgHooks;

public class SalesForceLandingPage extends TestNgHooks {
	
	/*public SalesForceLandingPage(RemoteWebDriver driver, WebDriverWait wait, Properties prop) {
		this.driver=driver;
		this.wait=wait;
		this.prop=prop;
	}*/
	
	public SalesForceLandingPage clickApplauncher() throws InterruptedException {
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("SalesForceLandingPage.appLauncher.xpath"))));
		//driver.findElement(By.xpath(prop.getProperty("SalesForceLandingPage.appLauncher.xpath"))).click();
		
		
		explictWait(By.xpath(getProp().getProperty("SalesForceLandingPage.appLauncher.xpath")), "visibilityOfElement");
		click(locateElement("xpath", getProp().getProperty("SalesForceLandingPage.appLauncher.xpath")));
		System.out.println("applauncher");
		return this;
	}
	
	public SalesForceLandingPage clickviewAll() throws InterruptedException {
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("SalesForceLandingPage.viewAll.xpath"))));
		//driver.findElement(By.xpath(prop.getProperty("SalesForceLandingPage.viewAll.xpath"))).click();
		
		explictWait(By.xpath(getProp().getProperty("SalesForceLandingPage.viewAll.xpath")), "visibilityOfElement");
		click(locateElement("xpath", getProp().getProperty("SalesForceLandingPage.viewAll.xpath")));
		System.out.println("viewAll");
		Thread.sleep(5000);
		return this;
		
	}
	
	public SalesForceHomePage enterAndClickSales() {
		/*driver.findElement(By.xpath(prop.getProperty("SalesForceLandingPage.serachItem.xpath"))).sendKeys("sale");
		System.out.println(driver.findElement(By.xpath(prop.getProperty("SalesForceLandingPage.salesItemtext.xpath"))).getText());
		//Select the sales App
		driver.findElement(By.xpath(prop.getProperty("SalesForceLandingPage.salesItemtext.xpath"))).click();
		System.out.println(driver.getTitle());*/
		
		type(locateElement("xpath", getProp().getProperty("SalesForceLandingPage.serachItem.xpath")), "sale");
		System.out.println(getText(locateElement("xpath", getProp().getProperty("SalesForceLandingPage.salesItemtext.xpath"))));
		click(locateElement("xpath", getProp().getProperty("SalesForceLandingPage.salesItemtext.xpath")));
		System.out.println(getTitle());
		//return new SalesForceHomePage(driver, wait, prop);
		return new SalesForceHomePage();

	}
	
	public ContractsRecentlyViewedPage scrollAndClickContracts() throws InterruptedException {
	/*	WebElement element = driver.findElement(By.xpath(prop.getProperty("SalesForceLandingPage.contracts.xpath"))); 
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].scrollIntoView(true);", element); 
		Thread.sleep(500);
		executor.executeScript("arguments[0].click();", element);*/
		
		executeJavaScript(locateElement("xpath",getProp().getProperty("SalesForceLandingPage.contracts.xpath")), "arguments[0].scrollIntoView(true);");
		executeJavaScript(locateElement("xpath",getProp().getProperty("SalesForceLandingPage.contracts.xpath")), "arguments[0].click();");
		//return new ContractsRecentlyViewedPage(driver,wait, prop);
		return new ContractsRecentlyViewedPage();

	}

}
