package week5pominplmentation.pages;

import week5pomImplementation.hooks.TestNgHooks;

public class SalesForceLoginPage extends TestNgHooks{
	
	/*public SalesForceLoginPage(RemoteWebDriver driver, WebDriverWait wait, Properties prop) {
		this.driver=driver;
		this.wait=wait;
		this.prop=prop;
		}*/
	
	
	public SalesForceLoginPage enterUsername(String username) {
		//driver.findElement(By.id(prop.getProperty("SalesForceLoginPage.username.id"))).sendKeys(username);
		
		type(locateElement("id", getProp().getProperty("SalesForceLoginPage.username.id")), username);
		return this;

	}
	
	public SalesForceLoginPage enterPassword(String password) {
		
		//driver.findElement(By.id(prop.getProperty("SalesForceLoginPage.password.id"))).sendKeys("BootcampSel$123");
		
		type(locateElement("id", getProp().getProperty("SalesForceLoginPage.password.id")), password);
		return this;
		
		}
	
	public SalesForceLandingPage clickLogin() {
		//driver.findElement(By.id(prop.getProperty("SalesForceLoginPage.login.id"))).click();
		
		click(locateElement("id", getProp().getProperty("SalesForceLoginPage.login.id")));
		//return new SalesForceLandingPage(driver, wait, prop);
		return new SalesForceLandingPage();
		
	}


}
