package week3TestNg;

import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseClass {
	
	RemoteWebDriver driver;
	WebDriverWait wait;
	public String fileName;
	public String sheetName;

	
	@BeforeMethod(groups = {"Tasks"})
	public void launchApp(String url, String username, String password, String def_dwnld_dir) {
		
		WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		
		HashMap<String, Object> prefs = new HashMap<>();
		prefs.put("plugins.always_open_pdf_externally", true);
		prefs.put("download.default_directory", def_dwnld_dir);
		options.setExperimentalOption("prefs", prefs);
		
		driver = new ChromeDriver(options);
	
		//1) Launch the browser
		//driver.get("https://login.salesforce.com/");
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		wait=new WebDriverWait(driver, 30);
		
		//2) Click Login
		//3) Login with the credentials
		driver.findElement(By.id("username")).sendKeys(username);
		driver.findElement(By.id("password")).sendKeys(password);
		driver.findElement(By.id("Login")).click();
				
	
	}
	
	@AfterMethod(groups = {"Tasks"})
	public void closeBrowser() {
		driver.close();
	}

	@DataProvider(name="FetchData")
	public String[][] sendData() throws IOException {
		return ReadExcel.readExcel(fileName, sheetName);
	}
}
