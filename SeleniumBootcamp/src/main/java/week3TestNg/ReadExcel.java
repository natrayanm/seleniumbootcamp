package week3TestNg;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	
	public static String[][] readExcel(String Filename, String sheetName) throws IOException {
		
		System.out.println("Filename is "+ Filename);

		XSSFWorkbook wb = new XSSFWorkbook(Filename);
		XSSFSheet ws = wb.getSheet(sheetName);
		
		int row_cnt = ws.getLastRowNum();
		int col_cnt = ws.getRow(0).getLastCellNum();
		
		String[][] data = new String[row_cnt][col_cnt];
		
		for(int i=1; i<=row_cnt; i++) {
			
			for(int j=0; j<col_cnt; j++){
				String cell_value = ws.getRow(i).getCell(j).getStringCellValue();
				data[i-1][j] = cell_value;
				System.out.println(cell_value);
			}
		}
		
		return data;
	}

}
