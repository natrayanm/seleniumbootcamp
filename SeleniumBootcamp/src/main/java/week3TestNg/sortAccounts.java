package week3TestNg;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.opentelemetry.exporter.logging.SystemOutLogExporter;

public class sortAccounts extends BaseClass {
	
	@Test(groups = {"Sorting"})
	public void sortAccounts() throws InterruptedException {
		// TODO Auto-generated method stub
		
		/*WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		
		ChromeDriver driver = new ChromeDriver(options);
	
		//1) Launch the browser
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		WebDriverWait wait=new WebDriverWait(driver, 20);
		
		
		//2) Click Login
		//3) Login with the credentials
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click(); */
		
		System.out.println("SortAccount");
				
		//4) Click on App launcher
		driver.findElement(By.xpath("//div[@class=\"slds-icon-waffle\"]")).click();
		//Click "View All"
		driver.findElement(By.xpath("//button[@aria-label=\"View All Applications\"]")).click();
		//Serach for application containing sale
		driver.findElement(By.xpath("//input[@placeholder=\"Search apps or items...\"]")).sendKeys("sale");
		System.out.println(driver.findElement(By.xpath("//p[@title=\"Manage your sales process with accounts, leads, opportunities, and more\"]")).getText());
		//Select the sales App
		driver.findElement(By.xpath("//p[@title=\"Manage your sales process with accounts, leads, opportunities, and more\"]")).click();
		
		//Click the accounts tab
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//one-app-nav-bar-item-root/a[@title=\"Accounts\"]")));
		WebElement element = driver.findElement(By.xpath("//one-app-nav-bar-item-root/a[@title=\"Accounts\"]"));
		
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
	    
	    //find the table
		driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]"));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@aria-label=\"Recently Viewed\"]")));
		
		Thread.sleep(3000);
		
		
		int elements_row_size = 0;
		List<String> account_names = new ArrayList<String>();
		
		//Iterate and get all account names
		for (int i=1; i<=200; i++) {
			try {
				//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).getText());
				account_names.add(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).getText());
				driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).sendKeys(Keys.DOWN);
				//System.out.println("record number "+i);
			}
			catch(NoSuchElementException e) {
				System.out.println("Record no while breaking "+ i);
				elements_row_size= i -1;
				break;
			}
				
			}
		
		System.out.println("Table row size is "+ elements_row_size);
		System.out.println("List size is "+ account_names.size());
		
		//Do a case insensitive sort
		Collections.sort(account_names, String.CASE_INSENSITIVE_ORDER);
		
		List<String> account_names_new = new ArrayList<String>();

		for (String string : account_names) {
			if (string.contentEquals("Credit") || string.contentEquals("Test")){
				continue;
			}
			////remove the name "Credit" as this sorting order is different in app [aAbB] and util [AaBb]
			account_names_new.add(string);
			System.out.println(string);
			
		}
		
		
		
		//Click the Account name to sort based on account name
		element = driver.findElement(By.xpath("//table/thead/tr/th[3]/div/a/span[text()=\"Account Name\"]"));
		executor.executeScript("arguments[0].click();", element);
		Thread.sleep(5000);
		
		List<String> account_names_sorted = new ArrayList<String>();
		List<String> account_names_sorted_new = new ArrayList<String>();

		//Iterate and the get the sorted account names from the table.
		for (int i=1; i<=200; i++) {
			try {
				//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).getText());
				account_names_sorted.add(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).getText());

				try {
					driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).sendKeys(Keys.DOWN);
				}
				catch(ElementNotInteractableException e) {
					System.out.println("Element not interctable");
					Thread.sleep(30000);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")));
					driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).sendKeys(Keys.DOWN,Keys.DOWN);
				}
				//System.out.println("record number "+i);
			}
			catch(NoSuchElementException e) {
				System.out.println("Record no while breaking "+ i); 
				elements_row_size= i -1;
				break;
			}
				
			}
		
		//remove the name "Credit"
		for (String string : account_names_sorted) {
			if (string.contentEquals("Credit") || string.contentEquals("Test")){
				continue;
			}
			account_names_sorted_new.add(string);
			System.out.println(string);
		}
		
		if(account_names_new.equals(account_names_sorted_new)) {
			System.out.println("Sorting is perfect");
		}
		else
			System.out.println("Sorting is wrong");
		
		driver.close();	
		
	}
	
}