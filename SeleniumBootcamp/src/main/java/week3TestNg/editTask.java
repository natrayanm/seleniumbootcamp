package week3TestNg;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class editTask extends BaseClass{

	@BeforeTest(groups = {"Tasks"})
	public void setFilename() {
		System.out.println("Calling Before test");
		fileName = "C:\\Natrayan\\Testleaf\\Docs\\ReadExcel.xlsx";
		sheetName="Sheet2";
		System.out.println("Filename in setfilename is "+fileName);
	}
	
	@Test(groups = {"Tasks"}, retryAnalyzer = RetryFailedTestCase.class, dataProvider="FetchData")
	public void editTasks(String dueDate, String priority) throws InterruptedException {
		
		System.out.println("Edit Task");


		/*WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		
		ChromeDriver driver = new ChromeDriver(options);
		
		
		//1) Launch the browser
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		
		//2) Click Login
		//3) Login with the credentials
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click(); */
		
		//4) Click on App launcher
		driver.findElement(By.xpath("//div[@class=\"slds-icon-waffle\"]")).click();
		//driver.findElement(By.xpath("//div[@class=\"slds-icon-waffle1\"]")).click();
		
		Thread.sleep(5000);
		//Click "View All"
		driver.findElement(By.xpath("//button[@aria-label=\"View All Applications\"]")).click();
		//Serach for application containing sale
		driver.findElement(By.xpath("//input[@placeholder=\"Search apps or items...\"]")).sendKeys("sale");
		System.out.println(driver.findElement(By.xpath("//p[@title=\"Manage your sales process with accounts, leads, opportunities, and more\"]")).getText());
		//Select the sales App
		driver.findElement(By.xpath("//p[@title=\"Manage your sales process with accounts, leads, opportunities, and more\"]")).click();
		
		//click task
		WebElement element = driver.findElement(By.xpath("//one-app-nav-bar-item-root[contains(@class,\"navItem\")]//a[@title=\"Tasks\"]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
	    
	  //Select recently viewed tab
	    Thread.sleep(3000);
	    driver.findElement(By.xpath("//h1//span[text()='Recently Viewed']")).click();
	    Thread.sleep(3000);
	  //Select recently viewed option in the dropdown
	    driver.findElement(By.xpath("//a[@role=\"option\"]//span[text()='Recently Viewed']")).click();
	    Thread.sleep(3000);
	   
	   
	    // Click the first task
	    element = driver.findElement(By.xpath("//a//span[text()='Bootcamp']"));
	    executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
	    
	    Thread.sleep(5000);
	    
	    //Edit Due date
	    driver.findElement(By.xpath("//button[@title=\"Edit Due Date\"]")).click();
	    // Clear the existing date
	    driver.findElement(By.xpath("//lightning-datepicker//input[@type=\"text\"]")).clear();
	    //Enter the date as string
	    driver.findElement(By.xpath("//lightning-datepicker//input[@type=\"text\"]")).sendKeys(dueDate);
	    //Come out of date field
	    Actions keyDown = new Actions(driver);
	    Thread.sleep(5000);
	    //this worked
	    keyDown.sendKeys(Keys.TAB, Keys.TAB).build().perform(); 
	    Thread.sleep(5000);
	    
	    //click the dropdown for priority
	    //element = driver.findElement(By.xpath("//div[contains(@class,\"uiInputSelect\")]//a[text()=\"Normal\"]"));
	    element = driver.findElement(By.xpath("(//div[@class=\"uiPopupTrigger\"]//a[@class=\"select\"])[2]"));
	    executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
	    Thread.sleep(5000);
	    
	    //select priority as low
	    driver.findElement(By.xpath("//div[@class=\"select-options\"]//a[@title=\"Low\"]")).click();
	    
	   //save the task
	    driver.findElement(By.xpath("//button[@type=\"button\"]//span[text()=\"Save\"]")).click();
	    Thread.sleep(5000);
	    
	  //click task
	    element = driver.findElement(By.xpath("//one-app-nav-bar-item-root[contains(@class,\"navItem\")]//a[@title=\"Tasks\"]"));
	  	executor = (JavascriptExecutor)driver;
	  	executor.executeScript("arguments[0].click();", element);
	  	    
	 //Select recently viewed tab
	  	Thread.sleep(5000);
	  	driver.findElement(By.xpath("//h1//span[text()='Recently Viewed']")).click();
	  	Thread.sleep(3000);
	  	
	 //Select recently viewed option in the dropdown
	  	driver.findElement(By.xpath("//a[@role=\"option\"]//span[text()='Recently Viewed']")).click();
	  	Thread.sleep(3000);
	  
	 // Click the first task
		//driver.findElement(By.xpath("//a//span[text()='Bootcamp']")).click();
		element = driver.findElement(By.xpath("//a//span[text()='Bootcamp']"));
		executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
	  	    
	 //Assert the changes done for priority
	    String name = driver.findElement(By.xpath("(//div[@class=\"slds-form-element__control slds-grid itemBody\"]//span/span)[4]")).getText();
	    System.out.println(name);
	    Assert.assertEquals(name, priority);
	    
	   // driver.close();

	}
}