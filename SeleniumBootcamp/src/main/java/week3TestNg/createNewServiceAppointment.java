package week3TestNg;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

public class createNewServiceAppointment extends BaseClass{
	
	@Test(groups = {"SA"})
	public void serviceAppointment() throws InterruptedException {
		
		//4) Click on App launcher
		driver.findElement(By.xpath("//div[@class=\"slds-icon-waffle\"]")).click();
		Thread.sleep(5000);
		//Click "View All"
		driver.findElement(By.xpath("//button[@aria-label=\"View All Applications\"]")).click();
		
		//scroll the page to element service aapointment
		WebElement element = driver.findElement(By.xpath("//span//p[text()=\"Service Appointments\"]")); 
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].scrollIntoView(true);", element); 
		Thread.sleep(500);
		executor.executeScript("arguments[0].click();", element);
		
		//Click new service appointment
		driver.findElement(By.xpath("//a[@title=\"New\"]/div[text()=\"New\"]")).click();
		
		//Enter description
		driver.findElement(By.xpath("//textarea[@role=\"textbox\"]")).sendKeys("Creating Service Appointments");
		
		//Click Search account in parent record
		driver.findElement(By.xpath("//div/input[@placeholder=\"Search Accounts...\"]")).click();
		
		//click create new account
		driver.findElement(By.xpath("//div[contains(@class,\"createNew\")]/span")).click();
		
		//enter new account name
		driver.findElement(By.xpath("//span[text()=\"Account Name\"]/parent::label/following-sibling::input")).sendKeys("Natrayan");
		
		//click save
		driver.findElement(By.xpath("(//button[@title=\"Save\"]/span[text()=\"Save\"])[2]")).click();
		
		//enter Earliest start date
		SimpleDateFormat formatter1 = new SimpleDateFormat("MM/dd/yyyy");
		
		Date dt = new Date();
		Calendar c = Calendar.getInstance(); 
		dt = c.getTime();
		String new_date = (formatter1.format(dt)).toUpperCase();
		driver.findElement(By.xpath("//label[text()=\"Date\"]/following-sibling::input")).sendKeys(new_date);
		
		//Calculate nearest time and enter
		
		int hr = c.get(Calendar.HOUR);
		int minute = c.get(Calendar.MINUTE);
		int min = minute%15;
		//System.out.println(min);
		if (min > 1) {
			minute = ((int)minute/15)*15 + 15;
		}
		//System.out.println(minute);
		
		String time="";
		if(c.get(Calendar.AM_PM)==1) {
		 time = Integer.toString(hr)	+ ":" + Integer.toString(minute) + " PM";
		}
		else
		 time = Integer.toString(hr)	+ ":" + Integer.toString(min) + " AM";
	
		for(int i=0; i<8; i++) {
			Thread.sleep(1000);
			driver.findElement(By.xpath("//div/label[text()=\"Time\"]/following-sibling::input")).sendKeys(Keys.BACK_SPACE);
		}
		driver.findElement(By.xpath("//div/label[text()=\"Time\"]/following-sibling::input")).sendKeys(time);
		
		
		c.setTime(dt); 
		c.add(Calendar.DATE, 5);
		dt = c.getTime();
		new_date = (formatter1.format(dt)).toUpperCase();
		System.out.println(new_date);
		driver.findElement(By.xpath("(//label[text()=\"Date\"]/following-sibling::input)[2]")).sendKeys(new_date);

		//click save
		driver.findElement(By.xpath("//button[@title=\"Save\"]/span[text()=\"Save\"]")).click();
		
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(@class,\"toastMessage\")]")));
	    String toast_msg = driver.findElement(By.xpath("//span[contains(@class,\"toastMessage\")]")).getText();
	    System.out.println(toast_msg);
	    
	    String sa_patern = "(SA-[0-9]+)";
	    
	    Pattern pattern = Pattern.compile(sa_patern);
	    Matcher matcher = pattern.matcher(toast_msg);
	    boolean matchFound = matcher.find();
	    String sa_id=matcher.group();
	    System.out.println(sa_id);
	    
	  //4) Click on App launcher
	  driver.findElement(By.xpath("//div[@class=\"slds-icon-waffle\"]")).click();
	  Thread.sleep(5000);
	  //Click "View All"
	  driver.findElement(By.xpath("//button[@aria-label=\"View All Applications\"]")).click();
	  		
	  //scroll the page to element service aapointment
	  element = driver.findElement(By.xpath("//span//p[text()=\"Service Appointments\"]")); 
	  executor = (JavascriptExecutor)driver;
	  executor.executeScript("arguments[0].scrollIntoView(true);", element); 
	  Thread.sleep(500);
	  executor.executeScript("arguments[0].click();", element);
	  		
	  Thread.sleep(2000);

	  driver.findElement(By.xpath("//input[@placeholder=\"Search this list...\"]")).sendKeys(sa_id, Keys.ENTER);
	  String sa_id_der = driver.findElement(By.xpath("//table/tbody/tr/th/span/a[text()=\"" + sa_id + "\"]")).getAttribute("title");
	  if(sa_id.equals(sa_id_der)) {
		  System.out.println("found the sa");
	  }
	  else
		  System.out.println("no sa found");
	  
	  driver.close();
	}

}
