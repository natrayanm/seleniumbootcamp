package week3TestNg;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class salesForceClassic_NewAccount extends BaseClass{
	
	@Test(groups = {"Others"})
	public void salesForce_NewAccount() throws InterruptedException {
		
		System.out.println("SaleForce New Account");
		
		//click the view profile
		Thread.sleep(3000);
		
		WebElement element = driver.findElement(By.xpath("//div/span[text()=\"View profile\"]")); 
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element); 
		
		
		//click and switch to salesforce classic
		driver.findElement(By.xpath("//div/a[text()=\"Switch to Salesforce Classic\"]")).click();
		System.out.println(driver.getTitle());
		
		//click create new option
		driver.findElement(By.xpath("//div/span[text()=\"Create New...\"]")).click();
		
		//Select Account option
		driver.findElement(By.xpath("//div[@id=\"createNew\"]/div/a[text()=\"Account\"]")).click();
		
		//Enter account name
		driver.findElement(By.xpath("//table[@class=\"detailList\"]/tbody/tr[2]/td[2]/div/input")).sendKeys("Bootcamp_Natrayan");
		
		//Enter address
		driver.findElement(By.xpath("(//table[@class=\"detailList\"]/tbody/tr[1]/td[2]/textarea)[1]")).sendKeys("ThackalStreet");
		driver.findElement(By.xpath("//table[@class=\"detailList\"]/tbody/tr[2]/td[2]/input")).sendKeys("Chennai");
		driver.findElement(By.xpath("(//table[@class=\"detailList\"]/tbody/tr[3]/td[2]/input)[7]")).sendKeys("WM");
		driver.findElement(By.xpath("(//table[@class=\"detailList\"]/tbody/tr[4]/td[2]/input)[2]")).sendKeys("CV24NY");
		driver.findElement(By.xpath("(//table[@class=\"detailList\"]/tbody/tr[5]/td[2]/input)[2]")).sendKeys("India");
		
		//Copy billing address to shipping address
		driver.findElement(By.xpath("//div/span/span/a[text()=\"Copy Billing Address to Shipping Address\"]")).click();
		
		//add 20 days to current date and input to SLA expiration date
		SimpleDateFormat formatter1 = new SimpleDateFormat("MM/dd/yyyy");
		
		Date dt = new Date();
		Calendar c = Calendar.getInstance(); 
		c.setTime(dt); 
		c.add(Calendar.DATE, 20);
		dt = c.getTime();
		String new_date = (formatter1.format(dt)).toUpperCase();
		System.out.println(new_date);
		driver.findElement(By.xpath("//table/tbody/tr[2]/td[2]/span/input")).sendKeys(new_date);
		
		Thread.sleep(2000);
		
		//save account
		driver.findElement(By.xpath("//table/tbody/tr[1]/td[2]/input[@name=\"save\"]")).click();
		
		//Get the account name created if its listed in the recent item
		String recent_item = driver.findElement(By.xpath("(//table/tbody/tr/td/div/div[2][contains(@class,\"recentItem\")]/div[2]/div/div/div/a/span)[1]")).getText();
		System.out.println("Recent Item is " + recent_item);
		Thread.sleep(2000);

	/*	//click the recently created account
		driver.findElement(By.xpath("(//table/tbody/tr/td/div/div[2][contains(@class,\"recentItem\")]/div[2]/div/div/div/a/span)[1]")).click();
				
		//Check the name
		Assert.assertEquals(recent_item, "Bootcamp_Natrayan"); */
		
		
		//List<WebElement> elements = driver.findElements(By.xpath("//table[@class=\"outer\"]/tbody/tr/td[@id=\"sidebarCell\"]/div/div[2]/div[2]/div/div/div/a"));
		//Iterate and get the list of all accounts under recent items
		List<WebElement> elements = driver.findElements(By.xpath("(//table/tbody/tr/td/div/div[2][contains(@class,\"recentItem\")]/div[2]/div/div/div/a/span)[1]"));
		String account_name="";
		for (WebElement element1 : elements) {
			Thread.sleep(1000);
			String recent_account = element1.getText();
			System.out.println("Inside loop is "+ element1.getText());
			if(recent_account.contains("Bootcamp_Natrayan")) {
				
				//click the account name that matches the given string
				driver.findElement(By.xpath("(//table/tbody/tr/td/div/div[2][contains(@class,\"recentItem\")]/div[2]/div/div/div/a/span)[1]")).click();
				//Get the account name  from the main pane
				account_name = driver.findElement(By.xpath("//table/tbody/tr[2]/td[2]/div")).getText();
				System.out.println("Account name in main pane is " +account_name);
				
				break;
			}
			
			
		}
		
		try {
			//assert the name from the main pane
			if (account_name.contains("Bootcamp_Natrayan")) {
				System.out.println();
			}	
		}
		catch(Exception e) {
			System.out.println(e);
		}
		
		//switch back to lightening experience page
		driver.findElement(By.xpath("//div[@class=\"linkElements\"]/a[text()=\"Switch to Lightning Experience\"]")).click();
		
		
		driver.close();
		
	}

}
