package week3TestNg;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class campaignUploadFile extends BaseClass{
	
	@Test(groups = {"Campaign"})
	public void  campaignUploadFiles() throws InterruptedException, AWTException {
		// TODO Auto-generated method stub
		
		/*WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		
		ChromeDriver driver = new ChromeDriver(options);
	
		//1) Launch the browser
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));

		
		//2) Login with the credentials
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click(); */
		
		System.out.println("Campaign Upload File");

		//4) Click on App launcher
		driver.findElement(By.xpath("//div[@class=\"slds-icon-waffle\"]")).click();
		//Click "View All"
		driver.findElement(By.xpath("//button[@aria-label=\"View All Applications\"]")).click();
		//Serach for application containing sale
		driver.findElement(By.xpath("//input[@placeholder=\"Search apps or items...\"]")).sendKeys("sale");
		System.out.println(driver.findElement(By.xpath("//p[@title=\"Manage your sales process with accounts, leads, opportunities, and more\"]")).getText());
		//Select the sales App
		driver.findElement(By.xpath("//p[@title=\"Manage your sales process with accounts, leads, opportunities, and more\"]")).click();
		
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//lightning-icon/lightning-primitive-icon)[15]")).click();
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a/span//span[text()=\"New Campaign\"]")));
		WebElement element = driver.findElement(By.xpath("//a/span//span[text()=\"New Campaign\"]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		
		driver.findElement(By.xpath("//label/span[text()=\"Campaign Name\"]/parent::label/following-sibling::input")).sendKeys("Natrayan");		

		SimpleDateFormat formatter1 = new SimpleDateFormat("MM/dd/yyyy");
		
		Date dt = new Date();
		Calendar c = Calendar.getInstance(); 
		c.setTime(dt); 
		c.add(Calendar.DATE, 1);
		dt = c.getTime();
		String tomorrow = (formatter1.format(dt)).toUpperCase();
		c.add(Calendar.DATE, 1);
		dt = c.getTime();
		String day_after = (formatter1.format(dt)).toUpperCase();

		driver.findElement(By.xpath("//label/span[text()=\"Start Date\"]/parent::label/following-sibling::div/input")).sendKeys(tomorrow);	
		driver.findElement(By.xpath("//label/span[text()=\"End Date\"]/parent::label/following-sibling::div/input")).sendKeys(day_after);	
		driver.findElement(By.xpath("//label/span[text()=\"End Date\"]/parent::label/following-sibling::div/input")).sendKeys(Keys.TAB);	

		
		driver.findElement(By.xpath("//button/span[text()=\"Save\"]")).click();
		
		Thread.sleep(3000);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(@class,\"toastMessage\")]")));
		    String campaign_toast = driver.findElement(By.xpath("//span[contains(@class,\"toastMessage\")]")).getText();
		    System.out.println(campaign_toast);
		    if (campaign_toast.contains("was created") && campaign_toast.contains("Campaign")) {
		    	System.out.println("contract is created");
		    }
		
		    Thread.sleep(5000);
		driver.findElement(By.xpath("//label/span[text()=\"Upload Files\"]")).click();
		
		Thread.sleep(2000); // suspending execution for specified time period
		 
	    // creating object of Robot class
	    Robot rb = new Robot();
	 
	    // copying File path to Clipboard
	    StringSelection str = new StringSelection("\"C:\\Users\\natra\\OneDrive\\Desktop\\Doc1.txt\"  \"C:\\Users\\natra\\OneDrive\\Desktop\\Doc2.txt\"");
	    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);
	 
	     // press Contol+V for pasting
	     rb.keyPress(KeyEvent.VK_CONTROL);
	     rb.keyPress(KeyEvent.VK_V);
	 
	    // release Contol+V for pasting
	    rb.keyRelease(KeyEvent.VK_CONTROL);
	    rb.keyRelease(KeyEvent.VK_V);
	 
	    // for pressing and releasing Enter
	    rb.keyPress(KeyEvent.VK_ENTER);
	    rb.keyRelease(KeyEvent.VK_ENTER);
	    
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button/span[text()=\"Done\"]")));
	    Thread.sleep(5000);
	    try {
		    driver.findElement(By.xpath("//button/span[text()=\"Done\"]")).click();
	     } catch (ElementClickInterceptedException e) {
	    	 System.out.println("xception is caught");
	        executor = (JavascriptExecutor) driver;
	        executor.executeScript("arguments[0].click();", driver.findElement(By.xpath("//button/span[text()=\"Done\"]")));
	     }
	    
	  List<WebElement> elements = driver.findElements(By.xpath("//div[contains(@data-divid,\"CombinedAttachment-file\")]/following-sibling::div/span"));
	for (WebElement webElement : elements) {
		System.out.println(webElement.getText());
	}		  
	
	driver.close();
	}

}
