package week3TestNg;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.opentelemetry.exporter.logging.SystemOutLogExporter;

public class deleteCampaignAttachment extends BaseClass{
	
	@Test (groups = {"Campaign"})
	public void deleteAttachment() throws InterruptedException {

		/*WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		
		ChromeDriver driver = new ChromeDriver(options);
	
		//1) Launch the browser
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));

		
		//2) Login with the credentials
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click(); */
		
		System.out.println("Deleting attachment from campaign");
		
		//4) Click on App launcher
		driver.findElement(By.xpath("//div[@class=\"slds-icon-waffle\"]")).click();
		//Click "View All"
		driver.findElement(By.xpath("//button[@aria-label=\"View All Applications\"]")).click();
		//Serach for application containing sale
		driver.findElement(By.xpath("//input[@placeholder=\"Search apps or items...\"]")).sendKeys("sale");
		System.out.println(driver.findElement(By.xpath("//p[@title=\"Manage your sales process with accounts, leads, opportunities, and more\"]")).getText());
		//Select the sales App
		driver.findElement(By.xpath("//p[@title=\"Manage your sales process with accounts, leads, opportunities, and more\"]")).click();
		
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a/span[text()=\"Campaigns\"]")));
		//Thread.sleep(8000);
		//driver.findElement(By.xpath("//a/span[text()=\"Campaigns\"]")).click();
		WebElement element = driver.findElement(By.xpath("//a/span[text()=\"Campaigns\"]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		
		int elements_row_size = 0;
		List<String> account_names = new ArrayList<String>();
		List<WebElement> elements = new ArrayList<WebElement>();

		
		//Iterate and get all account names
		for (int i=1; i<=5; i++) {
			try {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr[\"+ i +\"]/th/span/a")));
				Thread.sleep(4000);
				account_names.add(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).getText());
				Thread.sleep(4000);
					driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).click();
					Thread.sleep(4000);
					System.out.println("Name of the campaign is "+ account_names.get(i-1));
					elements = driver.findElements(By.xpath("//span[@title=\"Attachments\"]/following-sibling::span"));
					element = elements.get(elements.size()-1);
					
					//System.out.println("Atttribute is "+ driver.findElement(By.xpath("//span[@title=\"Attachments\"]/following-sibling::span")).getAttribute("title"));
					System.out.println(element.getAttribute("title"));
					String attribute = element.getAttribute("title");
					String attachment_val = attribute.replaceAll("[^0-9]", ""); // returns 123
					int no_of_atch = Integer.parseInt(attachment_val);
					if (no_of_atch > 0) {
						System.out.println("I found the attachments. " + no_of_atch);
						driver.findElement(By.xpath("//a/div/span/span[text()=\"Attachments\"]/parent::span")).click();
						for(int k=1; k<no_of_atch+1; k++) {
							driver.findElement(By.xpath("//table[@aria-label=\"Attachments\"]/tbody/tr[1]/td[5]/span/div/a/span")).click();
							Thread.sleep(5000);
							elements = 	driver.findElements(By.xpath("//div/ul/li[7]/a[@title=\"Delete\"]"));
							elements.get((elements.size()-1)).click();
							Thread.sleep(2000);
							driver.findElement(By.xpath("//button[@title=\"Delete\"]/span[text()=\"Delete\"]")).click();
							
							wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(@class,\"toastMessage\")]")));
						    String contract_toast = driver.findElement(By.xpath("//span[contains(@class,\"toastMessage\")]")).getText();
						    System.out.println(contract_toast);
						    if (contract_toast.contains("was deleted")) {
						    	System.out.println("attachment is deleted");
						    }
							Thread.sleep(5000);

						}	
						//check if all attachments are deleted by validating "No Items to Display"
						//TBD
						try{
							driver.findElement(By.xpath("//p/lightning-formatted-rich-text/span[text()=\"No items to display.\"]")).getText();
						}
						catch(Exception e) {
							System.out.println("Items are not deleted");
						}
					}
						
				Thread.sleep(4000);
				element = driver.findElement(By.xpath("//a/span[text()=\"Campaigns\"]"));
				executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", element);
				Thread.sleep(4000);
				driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).sendKeys(Keys.DOWN);
				
					
				}
					
					
				
			//}
			catch(NoSuchElementException e) {
				System.out.println("Record no while breaking "+ i);
				elements_row_size= i -1;
				break;
			}
				
			}
		
		

		
		System.out.println("Table row size is "+ elements_row_size);
		System.out.println("List size is "+ account_names.size());
		
		driver.close();
		
	}

}
