package week3TestNg;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

public class createFollowupEvent extends BaseClass{
	
	@Test(groups= {"Others"})
	public void createFollowupEvent() throws InterruptedException {
		
		System.out.println("CreateFollowupEvent Test");
		
		//4) Click on App launcher
		driver.findElement(By.xpath("//div[@class=\"slds-icon-waffle\"]")).click();
		Thread.sleep(5000);
		//Click "View All"
		driver.findElement(By.xpath("//button[@aria-label=\"View All Applications\"]")).click();
		//Serach for application containing sale
		driver.findElement(By.xpath("//input[@placeholder=\"Search apps or items...\"]")).sendKeys("content");
		
		Thread.sleep(1000);
		//select content option
		driver.findElement(By.xpath("//span/p/mark[text()=\"Content\"]")).click();
		
		//Click view all under Today's task
		driver.findElement(By.xpath("//div[contains(@class,\"activitiesTodayTaskContainer\")]/article/div[3]/a/span[text()=\"View All\"]")).click();
		
		//click "Display as" dropdown button
		driver.findElement(By.xpath("//div[@title=\"Display as Split View\"]/button[@title=\"Display as Split View\"]/lightning-primitive-icon[2]")).click();
		Thread.sleep(1000);

		//select the table option
		driver.findElement(By.xpath("//div[@title=\"Display as Split View\"]/div/ul/li/a/span[text()=\"Table\"]")).click();
		
		Thread.sleep(1000);

		//click the dropdown of first item under table
		driver.findElement(By.xpath("//table[@role=\"grid\"]/tbody/tr/td[7]/span/div/a/span/span[text()=\"Show Actions\"]/preceding-sibling::span")).click();
		
		Thread.sleep(1000);
		driver.findElement(By.xpath("//ul[@class=\"scrollable\"]/li/a[@title=\"Create Follow-Up Task\"]")).click();
		
		Thread.sleep(1000);
		//Click the dropdown for subject
		driver.findElement(By.xpath("(//div[@role=\"none\"]/input[@role=\"combobox\"])[2]")).click();
		Thread.sleep(2000);

		//driver.findElement(By.xpath("(//div[@role=\"none\"]/input[@role=\"combobox\"])[2]")).clear();
		//clear is not working
		String subject = driver.findElement(By.xpath("(//div[@role=\"none\"]/input[@role=\"combobox\"])[2]")).getAttribute("data-value");
		System.out.println("Subject is " + subject);
		for(int i=0; i<subject.length(); i++) {
			Thread.sleep(1000);
			driver.findElement(By.xpath("(//div[@role=\"none\"]/input[@role=\"combobox\"])[2]")).sendKeys(Keys.BACK_SPACE);
		}
		//driver.findElement(By.xpath("(//div[@role=\"none\"]/input[@role=\"combobox\"])[2]")).sendKeys(Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE);
		
		Thread.sleep(1000);
		//Enter subject as meeting
		driver.findElement(By.xpath("(//div[@role=\"none\"]/input[@role=\"combobox\"])[2]")).sendKeys("Meeting");
		
		try {
			driver.findElement(By.xpath("//input[@placeholder=\"Search Accounts...\"]")).click();

		}
		
		catch(ElementNotInteractableException e) {
			driver.findElement(By.xpath("(//a[@class=\"deleteAction\"])[3]")).click();
			Thread.sleep(1000);
		}
		
		
				
		//Select the second item in related to field
		//driver.findElement(By.xpath("//input[@placeholder=\"Search Accounts...\"]")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@placeholder=\"Search Accounts...\"]")).sendKeys(Keys.DOWN, Keys.DOWN, Keys.ENTER);
		
		//enter due date as next month 5th day
		SimpleDateFormat formatter1 = new SimpleDateFormat("MM/dd/yyyy");
		
		Date dt = new Date();
		Calendar c = Calendar.getInstance(); 
		c.setTime(dt); 
		c.add(Calendar.MONTH, 1);
		c.set(Calendar.DAY_OF_MONTH, 5);
		dt = c.getTime();
		String new_date = (formatter1.format(dt)).toUpperCase();
		System.out.println(new_date);
		
		driver.findElement(By.xpath("//button[@title=\"Select a date for Due Date\"]/parent::lightning-button-icon/parent::div/input")).sendKeys(new_date);
		
		//click save
		driver.findElement(By.xpath("//button[@title=\"Save\"]/span[text()=\"Save\"]")).click();
		
		//validate toast message
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(@class,\"toastMessage\")]")));
	    String task_toast = driver.findElement(By.xpath("//span[contains(@class,\"toastMessage\")]")).getText();
	    
	    System.out.println(task_toast);
	    if (task_toast.contains("Meeting") && task_toast.contains("was created")) {
	    	System.out.println("Meeting is created");
	    }
	    
	    //fetch the first record from table and check the due date
		Thread.sleep(3000);
	    String due_date= driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr/td[5]/span/span")).getText();
	    
	    System.out.println("Due date is "+due_date);
	    String formated_new_date = "";
		
		String[] date_split = new_date.split("/");
	    for (String string : date_split) {
	    	string = string.replaceFirst ("^0*", "");
	    	if (string.length()!=4)
	    	formated_new_date = formated_new_date  +string + "/";
	    	else
		    	formated_new_date = formated_new_date + string;
			System.out.println(string);
		}

		System.out.println("Formated new date is "+formated_new_date);
	   
	    if(due_date.equals(formated_new_date)){
	    	System.out.println("Meeting shcedule is correct");
	    }
	    else
	    	System.out.println("Meeting shcedule is wrong");
	    
	    driver.close();
	}

}
