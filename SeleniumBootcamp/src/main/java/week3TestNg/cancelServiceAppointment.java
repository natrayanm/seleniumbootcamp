package week3TestNg;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import io.opentelemetry.exporter.logging.SystemOutLogExporter;

public class cancelServiceAppointment extends BaseClass {
	
	@Test(groups= {"SA"})
	public void cancelServiceAppointment() throws InterruptedException {
		
		//4) Click on App launcher
		driver.findElement(By.xpath("//div[@class=\"slds-icon-waffle\"]")).click();
		Thread.sleep(5000);
		//Click "View All"
		driver.findElement(By.xpath("//button[@aria-label=\"View All Applications\"]")).click();
			
		//scroll the page to element service aapointment
		WebElement element = driver.findElement(By.xpath("//span//p[text()=\"Service Appointments\"]")); 
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].scrollIntoView(true);", element); 
		Thread.sleep(500);
		executor.executeScript("arguments[0].click();", element);
		
		int elements_row_size=0;
		String toast_msg = "";
		
		for (int i=1; i<=10; i++) {
			try {
				String status = driver.findElement(By.xpath("//table/tbody/tr["+i+"]/td[3]/span/span[1]")).getText();
				if(!status.equals("Canceled")) {
					
				    System.out.println("Status is "+ status);

					Thread.sleep(1000);
					driver.findElement(By.xpath("//table/tbody/tr["+i+"]/td[6]/span/div/a/span")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//li/a[@title=\"Cancel Service Appointment\"])")).click();
					
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[text()=\"Confirm Cancellation\"]")).click();
					
					Thread.sleep(2000);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(@class,\"toastMessage\")]")));
				    toast_msg = driver.findElement(By.xpath("//span[contains(@class,\"toastMessage\")]")).getText();
				    System.out.println("toast msg is "+toast_msg);
				    
				    break;
				}
				driver.findElement(By.xpath("//table/tbody/tr["+i+"]/th/span/a")).sendKeys(Keys.DOWN);
			}
			catch(NoSuchElementException e) {
				System.out.println("Record no while breaking "+ i);
				elements_row_size= i -1;
				break;
			}
				
			}
		
		
	    String sa_patern = "(SA-[0-9]+)";
	    
	    Pattern pattern = Pattern.compile(sa_patern);
	    Matcher matcher = pattern.matcher(toast_msg);
	    boolean matchFound = matcher.find();
	    String sa_id=matcher.group();
	    System.out.println(sa_id);
		
	    //4) Click on App launcher
		  driver.findElement(By.xpath("//div[@class=\"slds-icon-waffle\"]")).click();
		  Thread.sleep(5000);
		  //Click "View All"
		  driver.findElement(By.xpath("//button[@aria-label=\"View All Applications\"]")).click();
		  		
		  //scroll the page to element service aapointment
		  element = driver.findElement(By.xpath("//span//p[text()=\"Service Appointments\"]")); 
		  executor = (JavascriptExecutor)driver;
		  executor.executeScript("arguments[0].scrollIntoView(true);", element); 
		  Thread.sleep(500);
		  executor.executeScript("arguments[0].click();", element);
		  		
		  Thread.sleep(2000);

		  driver.findElement(By.xpath("//input[@placeholder=\"Search this list...\"]")).sendKeys(sa_id, Keys.ENTER);
		  String sa_id_status = driver.findElement(By.xpath("//table/tbody/tr[1]/td[3]/span/span[1]")).getText();
		  if(sa_id_status.equals("Canceled")) {
			  System.out.println("Cancelled the sa");
		  }
		  else
			  System.out.println("no sa found");
		  
		  driver.close();

	}

}
