package week3TestNg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class sortContract extends BaseClass{
	
	@Test(groups = {"Sorting"})
	public void sortContracts() throws InterruptedException {
		
		//4) Click on App launcher
		driver.findElement(By.xpath("//div[@class=\"slds-icon-waffle\"]")).click();
		Thread.sleep(5000);
		//Click "View All"
		driver.findElement(By.xpath("//button[@aria-label=\"View All Applications\"]")).click();
				
		//scroll the page to element contracts
		WebElement element = driver.findElement(By.xpath("//span//p[text()=\"Contracts\"]")); 
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].scrollIntoView(true);", element); 
		Thread.sleep(500);
		executor.executeScript("arguments[0].click();", element);
		
		Thread.sleep(3000);
		
		List<String> contract_no = new ArrayList<String>();
		List<String> contract_no_sorted = new ArrayList<String>();

		int elements_row_size = 0;


		//Iterate and get all contract numbers
		for (int i=1; i<=20; i++) {
			try {
				contract_no.add(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).getText());
				driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).sendKeys(Keys.DOWN);
				//System.out.println("record number "+i);
			}
			catch(NoSuchElementException e) {
				System.out.println("Record no while breaking "+ i);
				elements_row_size= i -1;
				break;
			}
				
			}
		
		for (String string : contract_no) {
			System.out.println("contract no is " +string);
		}
		
		Collections.sort(contract_no, String.CASE_INSENSITIVE_ORDER);
		System.out.println("This is after sorting....");
		for (String string : contract_no) {
			System.out.println("contract no is " +string);
		}
		
		driver.findElement(By.xpath("//table/thead/tr/th[@aria-label=\"Contract Number\"]/div/a")).click();
		Thread.sleep(3000);
		
		elements_row_size =0;
		
		for (int i=1; i<=20; i++) {
			try {
				contract_no_sorted.add(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).getText());
				driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).sendKeys(Keys.DOWN);
				//System.out.println("record number "+i);
			}
			catch(NoSuchElementException e) {
				System.out.println("Record no while breaking "+ i);
				elements_row_size= i -1;
				break;
			}
				
			}
		
		for (String string : contract_no_sorted) {
			System.out.println("contract no is " +string);
		}
		
		if(contract_no.equals(contract_no_sorted)) {
			System.out.println("sorting is perfect");
		}
		else
			System.out.println("sorting is wrong");

	}

}
