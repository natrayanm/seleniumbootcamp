package week3TestNg;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

public class createContract_wo_Mand_Param extends BaseClass{
	
	@Test(groups= {"Contracts"})
	public void createContractwihoutMandatoryParams() throws InterruptedException {
		
		//4) Click on App launcher
				driver.findElement(By.xpath("//div[@class=\"slds-icon-waffle\"]")).click();
				Thread.sleep(5000);
				//Click "View All"
				driver.findElement(By.xpath("//button[@aria-label=\"View All Applications\"]")).click();
				
				//scroll the page to element contracts
				WebElement element = driver.findElement(By.xpath("//span//p[text()=\"Contracts\"]")); 
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].scrollIntoView(true);", element); 
				Thread.sleep(500);
				executor.executeScript("arguments[0].click();", element);
				
				Thread.sleep(3000);
				//click new button to create a new contract
				driver.findElement(By.xpath("//a[@title='New']//div[text()='New']")).click();
				//Click the Account Name field
				driver.findElement(By.xpath("//input[@role='combobox'][@title=\"Search Accounts\"]")).click();
				
				//Click create New account
				
				//Select the name "Natrayan"
				try {
					driver.findElement(By.xpath("//div[@role='listbox']//div[@title='Natrayan3']")).click();

				}
				//catch(NoSuchElementException e) {
				catch(Exception e) {
					System.out.println("Exception is caught");
					driver.findElement(By.xpath("(//div[@role=\"listbox\"])[2]/div[3]//span[text()=\"New Account\"]")).click();
					driver.findElement(By.xpath("//h3/span[text()=\"Account Information\"]")).click();
					Thread.sleep(5000);
					driver.findElement(By.xpath("(//h3/following-sibling::div//input)[14]")).sendKeys("Natrayan3");
					driver.findElement(By.xpath("(//button[@title=\"Save\"]/span[text()=\"Save\"])[2]")).click();
				}
				
				Thread.sleep(4000);
				//Enter the contract start date
				driver.findElement(By.xpath("(//div[contains(@class,\"uiInput--datetime\")]//div[@class=\"form-element\"]//input)[1]")).clear();
				driver.findElement(By.xpath("(//div[contains(@class,\"uiInput--datetime\")]//div[@class=\"form-element\"]//input)[1]")).sendKeys("2/28/2022");
				
				//Come out of date field
			    Actions keyDown = new Actions(driver);
			    Thread.sleep(5000);
			    //this worked
			    keyDown.sendKeys(Keys.TAB, Keys.TAB).build().perform(); 
			    Thread.sleep(5000);
			    
			    driver.findElement(By.xpath("//button[@title=\"Save\"]//span[text()='Save']")).click();

			    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class=\"pageLevelErrors\"]/div[@role=\"alert\"]/div[@class=\"genericNotification\"]/span")));
			    String page_error = driver.findElement(By.xpath("//div[@class=\"pageLevelErrors\"]/div[@role=\"alert\"]/div[@class=\"genericNotification\"]/span")).getText();
			    System.out.println("Page error is "+page_error);
			    String feild_error = driver.findElement(By.xpath("//div[@class=\"pageLevelErrors\"]/div[@role=\"alert\"]/div[@class=\"genericNotification\"]/span/parent::div/following-sibling::ul/li")).getText();
			    System.out.println("Feild error is "+feild_error);
			    
			    if(feild_error.contains("Contract Term (months)")) {
			    	System.out.println("Correct error");
			    }
			    else
			    	System.out.println("Wrong results");
			    
			    driver.close();
	}

}
