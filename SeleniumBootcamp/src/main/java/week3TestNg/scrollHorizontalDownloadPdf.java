package week3TestNg;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.opentelemetry.exporter.logging.SystemOutLogExporter;

public class scrollHorizontalDownloadPdf extends BaseClass {
	
	@Parameters({"def_dwnld_dir"})
	@Test(groups= {"Certification"})
	public void downloadPdf(String def_dwnld_dir) throws InterruptedException {
		
		System.out.println("scrollHorizontalDownloadPdf");
		
		// to click the right scroll button
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class=\"rightScroll\"]/button/span[text()=\"Scroll Right\"]")));
		WebElement scroll = driver.findElement(By.xpath("//div[@class=\"rightScroll\"]/button/span[text()=\"Scroll Right\"]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		
		int main_loop=0;
		
		String parent=driver.getWindowHandle();

		for(int i=0; i<5; i++) 
		{
			//click the right scroll button
			executor.executeScript("arguments[0].click();", scroll); 
			//driver.executeAsyncScript("arguments[0].click();", scroll);
			Thread.sleep(1000);
			//Get the list of icons
			List<WebElement> elements = (driver.findElements(By.xpath("//div[@class=\"tileTitle\"]/h2/span")));
			for (WebElement webElement : elements) 
			{	
				//Get the title of each icon
				String icont_text = webElement.getText();
				//System.out.println(icont_text);
				// if icon contains release notes
				if (icont_text.equals("View Release Notes"))
				{
					main_loop=1;
					//webElement.click();
					//click get started
					driver.findElement(By.xpath("//div[@class=\"tileTitle\"]/h2/span[text()=\"View Release Notes\"]/ancestor::div[@class=\"tileTitle\"]/following-sibling::div[2]/button/span[text()=\"Get Started\"]")).click();
					System.out.println(icont_text);
					//get the list of windows
					Set<String>s=driver.getWindowHandles();
					// Now iterate using Iterator
					Iterator<String> I1= s.iterator();
					while(I1.hasNext())
					{
						String child_window=I1.next();
						if(!parent.equals(child_window))
							{
							//switch to child window in this case release notes window
							driver.switchTo().window(child_window);
							//reject titles
							driver.findElement(By.xpath("//div/button[text()=\"Reject All Cookies\"]")).click();
							System.out.println("Rejected cookies");
							//Get title of child window
							System.out.println("Title is "+ driver.switchTo().window(child_window).getTitle());
							Thread.sleep(5000);
							//click the dropdown to pick the release notes version
							executor.executeScript("arguments[0].click();", driver.findElement(By.xpath("//button[@name=\"releaseVersionPicker\"]/span")));
							Thread.sleep(2000);
							//select Spring'21 from the dropdown
							driver.findElement(By.xpath("//lightning-base-combobox-item/span/span[text()=\"Spring '21\"]")).click();
							wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//button[@title=\"Open PDF\"]"))));
							//click download option to download to preconfigured path
							driver.findElement(By.xpath("//button[@title=\"Open PDF\"]")).click();
							
							//Get the list of files in target download directory
							File dir = new File(def_dwnld_dir);
							  File[] dirContents = dir.listFiles();

							  //if no files available
							  if (dirContents.length ==0)
								  System.out.println("No file downloaded");
							  //if files downloaded
							  for (int k = 0; k < dirContents.length; k++) {
							      if (dirContents[k].getName().contains("release_notes") && dirContents[k].getName().contains("spring22")) {
							          // File has been found, it can now be deleted:
							          //dirContents[i].delete();
							          //return true;
							    	  System.out.println("File downloaded in correct path");
							      }
							      else{
							    	  System.out.println("File downloaded in wrong path");

							      }
							          }
							}
					}
					
					//if release notes is found break the inner loop
					break;
					
				}
				
			}
			//if release notes is found break the outer loop
			if (main_loop ==1) {
				System.out.println("main loop is "+main_loop);
				break;
			}
		}
		
		
		driver.close();
		driver.switchTo().window(parent);
		driver.close();
	}
	
	

}
