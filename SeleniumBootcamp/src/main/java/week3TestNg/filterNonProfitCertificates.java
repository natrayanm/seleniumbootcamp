package week3TestNg;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTAreaChart;
import org.testng.annotations.Test;

public class filterNonProfitCertificates extends BaseClass{
	
	@Test(groups ={"Certification"})
	public void filerCertificates() throws InterruptedException {
		
		System.out.println("filterNonProfitCertificates");
		
		// to click the right scroll button
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class=\"rightScroll\"]/button/span[text()=\"Scroll Right\"]")));
		WebElement scroll = driver.findElement(By.xpath("//div[@class=\"rightScroll\"]/button/span[text()=\"Scroll Right\"]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		
		int main_loop=0;
		int cnt=0;
		
		String parent=driver.getWindowHandle();

		for(int i=0; i<5; i++) 
		{
			//click the right scroll button
			executor.executeScript("arguments[0].click();", scroll); 
			//driver.executeAsyncScript("arguments[0].click();", scroll);
			Thread.sleep(1000);
			//Get the list of icons
			List<WebElement> elements = (driver.findElements(By.xpath("//div[@class=\"tileTitle\"]/h2/span")));
			for (WebElement webElement : elements) 
			{	
				//Get the title of each icon
				String icont_text = webElement.getText();
				//System.out.println(icont_text);
				// if icon contains release notes
				if (icont_text.equals("See System Status"))
				{
					main_loop=1;
					//webElement.click();
					//click get started
					driver.findElement(By.xpath("//div[@class=\"tileTitle\"]/h2/span[text()=\"See System Status\"]/ancestor::div[@class=\"tileTitle\"]/following-sibling::div[2]/button/span[text()=\"Get Started\"]")).click();
					System.out.println(icont_text);
					//get the list of windows
					Set<String>s=driver.getWindowHandles();
					// Now iterate using Iterator
					Iterator<String> I1= s.iterator();
					while(I1.hasNext())
					{
						String child_window=I1.next();
						if(!parent.equals(child_window))
							{
								//switch to child window in this case release notes window
								driver.switchTo().window(child_window);
								//reject titles
								driver.findElement(By.xpath("//div/button[text()=\"Reject All Cookies\"]")).click();
								System.out.println("Rejected cookies");
								//Get title of child window
								System.out.println("Title is "+ driver.switchTo().window(child_window).getTitle());
								//In the new window first click the dropdown
								driver.findElement(By.xpath("//div[@id=\"dropdown-list\"]/p[text()=\" Trust\"]/following-sibling::p[2]")).click();
								//In the new window select the Trust compliance option from dropdown
								driver.findElement(By.xpath("//span[@title=\"Menu Item Three\"]/p[text()=\" Trust  \"]/following-sibling::p[2]")).click();
								//Click show filter option
								driver.findElement(By.xpath("//div/button[text()=\" Show filters \"]")).click();
								//Select the non profit filter to list the non profit industries
								driver.findElement(By.xpath("//h2[text()=\"Industries\"]/parent::div/label[@for=\"Nonprofit\"]")).click();
								Thread.sleep(5000);
								
								elements = driver.findElements(By.xpath("//div/h2[@class=\"mb2 lh-title\"]/div/span"));
								for (WebElement webElement2 : elements) 
									{
										System.out.println(webElement2.getText());
										cnt++;
									}
							   System.out.println("Count of non profit certificates are "+ cnt);
							}
					}
					
					//if release notes is found break the inner loop
					break;
					
				}
				
			}
			//if release notes is found break the outer loop
			if (main_loop ==1) {
				System.out.println("main loop is "+main_loop);
				break;
			}
		}
		
		
		driver.close();
		driver.switchTo().window(parent);
		driver.close(); 
		
	}

}
