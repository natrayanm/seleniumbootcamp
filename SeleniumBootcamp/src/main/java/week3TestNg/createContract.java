package week3TestNg;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ISuite;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class createContract extends BaseClass{

	@Test(groups = {"Contracts"})
	public void createContract(ITestContext context) throws InterruptedException, ParseException {

		System.out.println("Create Contract");

		
		/*WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		
		ChromeDriver driver = new ChromeDriver(options);
	
		//1) Launch the browser
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		WebDriverWait wait=new WebDriverWait(driver, 20);
		
		//2) Click Login
		//3) Login with the credentials
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();*/
				
		//4) Click on App launcher
		driver.findElement(By.xpath("//div[@class=\"slds-icon-waffle\"]")).click();
		Thread.sleep(5000);
		//Click "View All"
		driver.findElement(By.xpath("//button[@aria-label=\"View All Applications\"]")).click();
		
		//scroll the page to element contracts
		WebElement element = driver.findElement(By.xpath("//span//p[text()=\"Contracts\"]")); 
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].scrollIntoView(true);", element); 
		Thread.sleep(500);
		executor.executeScript("arguments[0].click();", element);
		
		Thread.sleep(3000);
		//click new button to create a new contract
		driver.findElement(By.xpath("//a[@title='New']//div[text()='New']")).click();
		//Click the Account Name field
		driver.findElement(By.xpath("//input[@role='combobox'][@title=\"Search Accounts\"]")).click();
		
		//Click create New account
		
		//Select the name "Natrayan"
		try {
			driver.findElement(By.xpath("//div[@role='listbox']//div[@title='Natrayan3']")).click();

		}
		//catch(NoSuchElementException e) {
		catch(Exception e) {
			System.out.println("Exception is caught");
			driver.findElement(By.xpath("(//div[@role=\"listbox\"])[2]/div[3]//span[text()=\"New Account\"]")).click();
			driver.findElement(By.xpath("//h3/span[text()=\"Account Information\"]")).click();
			Thread.sleep(5000);
			driver.findElement(By.xpath("(//h3/following-sibling::div//input)[14]")).sendKeys("Natrayan3");
			driver.findElement(By.xpath("(//button[@title=\"Save\"]/span[text()=\"Save\"])[2]")).click();
		}
		
		Thread.sleep(4000);
		//Enter the contract start date
		driver.findElement(By.xpath("(//div[contains(@class,\"uiInput--datetime\")]//div[@class=\"form-element\"]//input)[1]")).clear();
		driver.findElement(By.xpath("(//div[contains(@class,\"uiInput--datetime\")]//div[@class=\"form-element\"]//input)[1]")).sendKeys("2/28/2022");
		
		//Come out of date field
	    Actions keyDown = new Actions(driver);
	    Thread.sleep(5000);
	    //this worked
	    keyDown.sendKeys(Keys.TAB, Keys.TAB).build().perform(); 
	    Thread.sleep(5000);
	    
	    //enter the contract months
	    driver.findElement(By.xpath("//div/div/div/div/input[@class=\"input uiInputSmartNumber\"]")).sendKeys("6");
	    driver.findElement(By.xpath("//button[@title=\"Save\"]//span[text()='Save']")).click();
	    
	    //capture the toast message and extract the contract id
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(@class,\"toastMessage\")]")));
	    String contract_toast = driver.findElement(By.xpath("//span[contains(@class,\"toastMessage\")]")).getText();
	    System.out.println(contract_toast);
	    if (contract_toast.contains("was created")) {
	    	System.out.println("contract is created");
	    }
	    	    
	    //Extract the contract id
	    String contract_patern = "([0-9]+)";
	    
	    Pattern pattern = Pattern.compile(contract_patern);
	    Matcher matcher = pattern.matcher(contract_toast);
	    boolean matchFound = matcher.find();
	    String contract_id=matcher.group();
	    if(matchFound) {
	      System.out.println("Match found " + contract_id);
	    } else {
	      System.out.println("Match not found");
	    }
	    Thread.sleep(5000);
		System.out.println("//a[@title=\"" + contract_id + "\"]/span[text()=\"" + contract_id + "\"]");
		
		ISuite suite = context.getSuite();
		suite.setAttribute("contract_id", contract_id);
		
		//Click the newly created contract link to get into the page where contracts are listed
		element = driver.findElement(By.xpath("//a[@title=\"" + contract_id + "\"]/span[text()=\"" + contract_id + "\"]"));
		executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
	    
	    //find the table
		driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]"));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@aria-label=\"Recently Viewed\"]")));
		
		Thread.sleep(3000);
		
		//Get the list of rows
		List<WebElement> elements_row = driver.findElements(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr"));
		System.out.println("Rows in table are " + elements_row.size());
		
		//iterate rows and find the name, contract_id, start_date and end_date for each contract/row
		for (int i=1; i<=elements_row.size(); i++) {
			//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).getText());
			String contract_id_tb = driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).getText();
			//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[3]/span/a")).getText());
			String contract_name = driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[3]/span/a")).getText();
			//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[5]/span")).getText());
			String contract_st_date = driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[5]/span")).getText();
			//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[6]/span")).getText());
			String contract_end_date=driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[6]/span")).getText();
			System.out.println("Contract Name is "+contract_name+ " Contract Id is "+contract_id_tb+" contract start date is "+ contract_st_date + " contract end date is "+contract_end_date);
			
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			
			Date d1 = sdf.parse(contract_st_date);
	        Date d2 = sdf.parse(contract_end_date);
	        long difference_In_Time = d2.getTime() - d1.getTime();
	        
	        long difference_In_Months = TimeUnit.MILLISECONDS.toDays(difference_In_Time)/30l;
	        
	        System.out.println("Difference in months is "+ difference_In_Months);
	        
			if (contract_name.equals("Natrayan")) {
				System.out.println("contract is created");
			}
			

			
		}
		
	    driver.close();

	}
	
}
