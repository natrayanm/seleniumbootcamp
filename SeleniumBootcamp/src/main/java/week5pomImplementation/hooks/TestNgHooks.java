package week5pomImplementation.hooks;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.ITestNGMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import com.fasterxml.jackson.databind.ObjectMapper;

import week3TestNg.ReadExcel;
import week5pominplmentation.base.BaseDriver;
import week5pominplmentation.base.ReadExcelSheet;

public class TestNgHooks extends BaseDriver{
	
	public static Map<String, String> dataMap = new LinkedHashMap<String, String>();

	@BeforeMethod(groups= {"Common"})
	@Parameters({ "browser", "url", "def_dwnld_dir" })
	public void setupBrowser(String browser, String url, String def_dwnld_dir) throws IOException {
		System.out.println("Browser is ");
		startApp(browser, url, def_dwnld_dir);
		
	}
	
	@AfterMethod(groups= {"Common"})
	public void closeBrowser() {
		getDriver().close();
	} 

	@DataProvider(name="FetchData")
	public String[][] sendData() throws IOException {
		return ReadExcel.readExcel(fileName, sheetName);
	}
	
	@DataProvider(name="FetchDataForEachTest")
	public String[][] sendDatanew(ITestNGMethod testContext) throws IOException {
		@SuppressWarnings("resource")
		XSSFWorkbook wb = new XSSFWorkbook(fileName);
		XSSFSheet ws = wb.getSheet(testContext.getMethodName());
		
		if( ws == null) {
			return ReadExcelSheet.readExcel(fileName, sheetName);
		}
		else
			return ReadExcelSheet.readExcel(fileName, testContext.getMethodName());

		
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> sendDataNewCucumber(String scenario) throws IOException {
		String[] names = scenario.split(":");
		//String[] names = str.split(":");
		//System.out.println(names[0]);
		//System.out.println((names[1].split("_"))[1]);

		fileName = "C:\\Natrayan\\Testleaf\\Docs\\"+names[0]+".xlsx";
		sheetName= (names[1].split("_"))[1];


		@SuppressWarnings("resource")
		XSSFWorkbook  wb =  new XSSFWorkbook(fileName);
		XSSFSheet ws = wb.getSheet(sheetName);

		Row row = ws.getRow(0);
		int lastcolumnused = row.getLastCellNum();
		int colnum = 0;
		for (int i = 0; i < lastcolumnused; i++) {
			if (row.getCell(i).getStringCellValue().equalsIgnoreCase("TC-01")) {
				colnum = i;
				break;
			}
		}


		for(int i=1; i<= wb.getSheet(sheetName).getLastRowNum(); i++) {
			row = ws.getRow(i);
			Cell column = row.getCell(colnum);
			String cellValue = column.getStringCellValue();
			System.out.println(cellValue);

			ObjectMapper objectMapper = new ObjectMapper();
			try {    
				dataMap = objectMapper.readValue(cellValue, Map.class);   
			}catch (Exception e) { 
				e.printStackTrace();
			}
			System.out.println("Map is " + dataMap);
			System.out.println("Map Size is " + dataMap.size());
		}

		return dataMap;

	}



}
