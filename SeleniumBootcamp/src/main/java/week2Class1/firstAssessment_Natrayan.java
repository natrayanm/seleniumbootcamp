package week2Class1;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class firstAssessment_Natrayan {

	public static void main(String[] args) throws InterruptedException {

		WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		
		ChromeDriver driver = new ChromeDriver(options);
	
		//1) Launch the browser
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));

		
		//2) Login with the credentials
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
				
		//3) Click on App launcher
		driver.findElement(By.xpath("//div[@class=\"slds-icon-waffle\"]")).click();
		//Click "View All"
		driver.findElement(By.xpath("//button[@aria-label=\"View All Applications\"]")).click();
		
		//4) Select service console option
		driver.findElement(By.xpath("//span/p[text()='Service Console']")).click();
		
		//5) Select home option from dropdown
		//click the dropdown
		WebElement element = driver.findElement(By.xpath("//button[@title=\"Show Navigation Menu\"]")); 
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		//select home from dropdown
		//driver.findElement(By.xpath("//div[@role=\"listbox\"]//span[text()=\"Home\"]")).click();
		
		//6 need not implement
		
		//7 Select dashboard from dropdown
		driver.findElement(By.xpath("//div[@role=\"listbox\"]//span[text()=\"Dashboards\"]")).click();
		
		//8 Click on New Dashboard 
		driver.findElement(By.xpath("//a[@title=\"New Dashboard\"]/div[text()=\"New Dashboard\"]")).click();
		
		/*//Switch to 
		driver.findElement(By.tagName("iframe"));
		//Wait before switching to frame. Explicit wait not working
		Thread.sleep(7000);
		//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//iframe[@title=\"dashboard\"]")));
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title=\"dashboard\"]")));
		System.out.println("Switched to frame");*/
		
		List<WebElement> framelist = driver.findElements(By.tagName("iframe"));
		for(int i=0; i<framelist.size();i++)
		{
			if(framelist.get(i).getAttribute("title").equalsIgnoreCase("Dashboard"))
			{
				Thread.sleep(6000);
				driver.switchTo().frame(i);
				System.out.println("entered into frame");
			}
			
		}
		
		//Wait before entering name, for element to be available. explicit wait not working
		Thread.sleep(10000);
		
		//9 Enter the Dashboard name as "YourName_Workout" 
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div/input[@id=\"dashboardNameInput\"]")));
		driver.findElement(By.xpath("//div/input[@id=\"dashboardNameInput\"]")).sendKeys("Natrayan_Workout");
		
		//10 Enter Description as Testing and Click on Create 
		driver.findElement(By.xpath("//div/input[@id=\"dashboardDescriptionInput\"]")).sendKeys("Testing");

		//click create
		driver.findElement(By.xpath("//footer/button[text()=\"Create\"]")).click();
		
		//wait for page to refresh
		Thread.sleep(5000);

		//switch to page
		driver.switchTo().defaultContent();
		
		//wait before switching to frame. explicit wait not working
		Thread.sleep(5000);
		
		//Switch to frame
		driver.findElement(By.tagName("iframe"));
		Thread.sleep(7000);

		//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//iframe[@title=\"dashboard\"]")));
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title=\"dashboard\"]")));
		System.out.println("Switched to frame");
		
		//11 Click Done
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[text()=\"Done\"]")));
		driver.findElement(By.xpath("//button[text()=\"Done\"]")).click();
		
		//switch to page
		Thread.sleep(5000);
		driver.switchTo().defaultContent();
				
		//12 Verify the Dashboard is Created
		driver.findElement(By.tagName("iframe"));
		Thread.sleep(7000);
		//wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//iframe[@title=\"dashboard\"]")));
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title=\"dashboard\"]")));
		System.out.println("Switched to frame");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1/span[text()=\"Dashboard\"]/following-sibling::span")));
		String dash_name = driver.findElement(By.xpath("//h1/span[text()=\"Dashboard\"]/following-sibling::span")).getText();
		System.out.println(dash_name);
		
		
		
		//13 Click on Subscribe 
		driver.findElement(By.xpath("//div/button[text()='Subscribe']")).click();
		driver.switchTo().defaultContent();
		Thread.sleep(4000);

		//14. Select Frequency as "Daily" 
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()=\"Daily\"]")));
		driver.findElement(By.xpath("//span[text()=\"Daily\"]")).click();
		
		//15 Time as 10:00 AM 16. Click on Save 
		Select se = new Select(driver.findElement(By.xpath("//select[@id=\"time\"]")));
		se.selectByValue("10");
		
		//click save
		driver.findElement(By.xpath("//span[text()=\"Save\"]")).click();
		
		//17. Verify "You started Dashboard Subscription" message displayed or not 
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,\"toastContent\")]//span[text()=\"You started a dashboard subscription.\"]")));
		String toast_msg = driver.findElement(By.xpath("//div[contains(@class,\"toastContent\")]//span[text()=\"You started a dashboard subscription.\"]")).getText();
		System.out.println(toast_msg);
		Thread.sleep(4000);
		
		//18. Close the "YourName_Workout" tab 
		//driver.findElement(By.xpath("//span[@title=\"New Dashboard\"]/following-sibling::span[text()=\"Natrayan_Workout\"]/ancestor::a/following-sibling::div[2]")).click();
		driver.findElement(By.xpath("//div/button[@title=\"Close Natrayan_Workout\"]")).click();
		
	
		//19. Click on Private Dashboards 
		driver.findElement(By.xpath("//li/a[text()=\"Private Dashboards\"]")).click();
		
		//20. Verify the newly created Dashboard available 
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder=\"Search private dashboards...\"]")));
		driver.findElement(By.xpath("//input[@placeholder=\"Search private dashboards...\"]")).clear();
		driver.findElement(By.xpath("//input[@placeholder=\"Search private dashboards...\"]")).sendKeys("Natrayan_Workout");
		
		
		Thread.sleep(3000);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@role=\"grid\"]/tbody/tr")));
		List<WebElement> elements_row = driver.findElements(By.xpath("//table[@role=\"grid\"]/tbody/tr"));
		System.out.println("Rows in table are " + elements_row.size());
		
		//iterate rows and find the name, contract_id, start_date and end_date for each contract/row
		for (int i=1; i<=elements_row.size(); i++) {
			//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).getText());
			//String pvt_dash_name = driver.findElement(By.xpath("//tbody/tr[" + i+ "]//span[text()=\"Natrayan_Workout\"]")).getText();
			String pvt_dash_name = driver.findElement(By.xpath("//tbody/tr//span[text()=\"Natrayan_Workout\"]")).getText();
			System.out.println(pvt_dash_name);		
		
		
		//21. Click on dropdown for the item 
		driver.findElement(By.xpath("//tbody/tr/td[6]//button")).click();
		
		//22 select delete option
		driver.findElement(By.xpath("//tbody/tr/td[6]//span[text()=\"Delete\"]")).click();
		
		//23 confirm delete
		driver.findElement(By.xpath("//button[@title=\"Delete\"]/span[text()=\"Delete\"]")).click();
		//wait for toast message
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(@class,\"toastMessage\")]")));
		toast_msg = driver.findElement(By.xpath("//span[contains(@class,\"toastMessage\")]")).getText();
		
		if (toast_msg.contains("was deleted")) {

			System.out.println("private dashboard deleted");
		}
		Thread.sleep(2000);
		
		}
		
		//24  Verify the item is not available under Private Dashboard folder
		String result = driver.findElement(By.xpath("//span[@class=\"emptyMessageTitle\"]")).getText();
		
		if (result.contains("No results found")){
			System.out.println("Deletion successful");
		}


	}

}
