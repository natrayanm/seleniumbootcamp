package week2Class1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;  


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.opentelemetry.exporter.logging.SystemOutLogExporter;

public class sortLegalEntities {

	public static void main(String[] args) throws InterruptedException {

		WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		
		ChromeDriver driver = new ChromeDriver(options);
	
		//1) Launch the browser
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		WebDriverWait wait=new WebDriverWait(driver, 20);
		
		
		//2) Click Login
		//3) Login with the credentials
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
				
		//4) Click on App launcher
		driver.findElement(By.xpath("//div[@class=\"slds-icon-waffle\"]")).click();
		//Click "View All"
		driver.findElement(By.xpath("//button[@aria-label=\"View All Applications\"]")).click();
		
		//5) Scroll and click Legal entities
		WebElement element = driver.findElement(By.xpath("//span/p[text()=\"Legal Entities\"]")); 
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].scrollIntoView(true);", element); 
		Thread.sleep(2000);
		executor.executeScript("arguments[0].click();", element);
		
		//extract records from table before sorting
		List<String> entity_modified_dates = new ArrayList<String>();
		int elements_row_size = 0;
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table/thead/tr[1]/th[4]/div/div/button")));
		
		//iterate table, extract last_modified_date and convert them into standard convention and add it to list
		for (int i=1; i<=100; i++) {
			try {
				String modified_date = driver.findElement(By.xpath("//table/tbody/tr["+i+"]/td[3]/span")).getText();
				//normalize the date format to MM/dd/yyyy, hh:mm a
				SimpleDateFormat sdf3 = new SimpleDateFormat("MM/dd/yyyy, hh:mm a");
		    	try {
		    		//convert the date from string format to the required Date format
					Date date4 = sdf3.parse(modified_date);
					//convert the required Date format back to string and add it to list
					entity_modified_dates.add((sdf3.format(date4)).toUpperCase());
					
					
				} catch (ParseException e) {
					
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table/tbody/tr["+i+"]/th/span/a")));
				driver.findElement(By.xpath("//table/tbody/tr["+i+"]/th/span/a")).sendKeys(Keys.DOWN);
			}
			catch(NoSuchElementException e) {
				System.out.println("Record no while breaking "+ i);
				elements_row_size= i -1;
				break;
			}
				
			}
		
		List<String> entity_modified_dates_new = new ArrayList<String>();

		 DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/uuuu',' hh:mm a", Locale.ENGLISH);

		 //sort the date in natural order [Ascending]
   		 List<LocalDateTime> dateTimeList = entity_modified_dates.stream()
   		         .map(ds -> LocalDateTime.parse(ds, formatter))
   		         .collect(Collectors.toCollection(ArrayList::new));
   		 
   		//after sorting date format is yyyy-MM-dd'T'HH:mm		
   		 dateTimeList.sort(Comparator.naturalOrder());
   				
   				 //Iterate the sorted date and convert it into required string format MM/dd/yyyy, hh:mm a
   				 for (LocalDateTime localDateTime : dateTimeList) {
   					SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
   					SimpleDateFormat sdf3 = new SimpleDateFormat("MM/dd/yyyy, hh:mm a");
   			    	try {
   						Date date4 = sdf2.parse(localDateTime.toString());
   						//after converting into required string format add into list
   						entity_modified_dates_new.add((sdf3.format(date4)).toUpperCase());
      					 System.out.println("Records sorted after fetching from table " + (sdf3.format(date4)).toUpperCase());

   						
   						
   					} catch (ParseException e) {
   						
   						// TODO Auto-generated catch block
   						e.printStackTrace();
   					}

	}

		
		//click the last modified date to sort by ascending order
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table/thead/tr[1]/th[4]/div/div/button")));
		element = driver.findElement(By.xpath("//span[text()=\"Last Modified Date\"]"));
		executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr")));
		Thread.sleep(3000);
		executor.executeScript("arguments[0].click();", element);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr")));
		Thread.sleep(3000);
		
		elements_row_size = 0;
		List<String> entity_modified_dates_sorted = new ArrayList<String>();
		//List<String> listDate =new ArrayList<String>();

		//Iterate and get all account names
		for (int i=1; i<=100; i++) {
			try {
				String modified_date = driver.findElement(By.xpath("//table/tbody/tr["+i+"]/td[3]/span")).getText();
				//Normalize the date into required date format MM/dd/yyyy, hh:mm a
				SimpleDateFormat sdf3 = new SimpleDateFormat("MM/dd/yyyy, hh:mm a");
		    	try {
					Date date4 = sdf3.parse(modified_date);
					//After normalization convert the date into required string format
					entity_modified_dates_sorted.add((sdf3.format(date4)).toUpperCase());
					
					
				} catch (ParseException e) {
					
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table/tbody/tr["+i+"]/th/span/a")));
				driver.findElement(By.xpath("//table/tbody/tr["+i+"]/th/span/a")).sendKeys(Keys.DOWN);
			}
			catch(NoSuchElementException e) {
				System.out.println("Record no while breaking "+ i);
				elements_row_size= i -1;
				break;
			}
		}
		
		for (String string : entity_modified_dates_sorted) {
			System.out.println("Records sorted in table before fetching " + string);
		}
	
			if (entity_modified_dates_new.equals(entity_modified_dates_sorted)) {
				System.out.println("Sorting is perfect");
			}
			else
				System.out.println("Sorting is wrong");
				 
	}
	
	

}
