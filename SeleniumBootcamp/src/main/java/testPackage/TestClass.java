package testPackage;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		SimpleDateFormat formatter1 = new SimpleDateFormat("MM/dd/yyyy");
		
		Date dt = new Date();
		Calendar c = Calendar.getInstance(); 
		c.setTime(dt); 
		c.add(Calendar.MONTH, 1);
		c.set(Calendar.DAY_OF_MONTH, 5);
		dt = c.getTime();
		String new_date = (formatter1.format(dt)).toUpperCase();
		System.out.println(new_date);
		String formated_new_date = "";
		
		String[] date_split = new_date.split("/");
	    for (String string : date_split) {
	    	string = string.replaceFirst ("^0*", "");
	    	if (string.length()!=4)
	    	formated_new_date = formated_new_date  +string + "/";
	    	else
		    	formated_new_date = formated_new_date + string;
			System.out.println(string);
		}

		System.out.println(formated_new_date);
		
		System.out.println(c.get(Calendar.HOUR) + " : " + c.get(Calendar.MINUTE) + " " +c.get(Calendar.AM_PM));
		
		int hr = c.get(Calendar.HOUR);
		int minute = c.get(Calendar.MINUTE);
		int min = minute%15;
		System.out.println(min);
		if (min > 1) {
			minute = ((int)minute/15)*15 + 15;
		}
		System.out.println(minute);
		
		String time="";
		if(c.get(Calendar.AM_PM)==1) {
		 time = Integer.toString(hr)	+ ":" + Integer.toString(minute) + " PM";
		}
		else
		 time = Integer.toString(hr)	+ ":" + Integer.toString(min) + " AM";
	
		System.out.println(time);
		
		String toast_msg = "Service Appointment \"SA-0064\" was created.";
		//System.out.println(toast_msg.matches("(SA-[0-9]+)"));
		
		 String sa_patern = "(SA-[0-9]+)";
		    
		    Pattern pattern = Pattern.compile(sa_patern);
		    Matcher matcher = pattern.matcher(toast_msg);
		    boolean matchFound = matcher.find();
		    String sa_id=matcher.group();
		    System.out.println(sa_id);
		   

	}

}
