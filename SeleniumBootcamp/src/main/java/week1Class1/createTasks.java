package week1Class1;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import io.github.bonigarcia.wdm.WebDriverManager;

public class createTasks {

	public static void main(String[] args) throws InterruptedException {

		WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		
		ChromeDriver driver = new ChromeDriver(options);
	
		//1) Launch the browser
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		WebDriverWait wait=new WebDriverWait(driver, 20);
		
		
		//2) Click Login
		//3) Login with the credentials
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
				
		//4) Click on App launcher
		driver.findElement(By.xpath("//div[@class=\"slds-icon-waffle\"]")).click();
		//Click "View All"
		driver.findElement(By.xpath("//button[@aria-label=\"View All Applications\"]")).click();
		//Serach for application containing sale
		driver.findElement(By.xpath("//input[@placeholder=\"Search apps or items...\"]")).sendKeys("sale");
		System.out.println(driver.findElement(By.xpath("//p[@title=\"Manage your sales process with accounts, leads, opportunities, and more\"]")).getText());
		//Select the sales App
		driver.findElement(By.xpath("//p[@title=\"Manage your sales process with accounts, leads, opportunities, and more\"]")).click();
		
		//Expand the task menu
		driver.findElement(By.xpath("//one-app-nav-bar-item-root[@data-id='0Qk2w000003YY0wCAG']//div")).click();
		//WebElement element = driver.findElement(By.xpath("//div//slot//one-app-nav-bar-menu-item//span[text()='New Task']"));
		//(//lightning-icon/lightning-primitive-icon)[11]
		
		//5) Click New Task
		WebElement element = driver.findElement(By.xpath("//span[text()='New Task']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
	    
	    //6) Enter subject as "Bootcamp " 
	    driver.findElement(By.xpath("//div//input[@class=\"slds-combobox__input slds-input\"]")).sendKeys("Bootcamp");
	    
	    //we cannot use Select command to select dropdowns which are not built by using "select" tag, so below code wont work
	    //Select dropdown = new Select(driver.findElement(By.xpath("(//div/div[@class=\"select-options\"])[2]")));
	    //dropdown.selectByVisibleText("Waiting on someone else");
	    
	   
	    //8) Click status Tab
	    driver.findElement(By.xpath("(//div/div//a[@class=\"select\"])[1]")).click();
	    element = driver.findElement(By.xpath("//div/ul//li//a[text()=\"Waiting on someone else\"]"));
		executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
	    
	    Thread.sleep(5000);
	    
	    //7) Select Contact from DropDown
	    driver.findElement(By.xpath("//input[@title=\"Search Contacts\"]")).click();
	    Actions keyDown = new Actions(driver);
		keyDown = new Actions(driver);
	    Thread.sleep(5000);
	    //this worked
	    keyDown.sendKeys(Keys.ARROW_DOWN, Keys.RETURN).build().perform(); 
	    
	  //9) Save the 'Task created' message
	    Thread.sleep(5000);
	    driver.findElement(By.xpath("//button[@title=\"Save\"]")).click();
		//driver.close();
	    //Thread.sleep(3000);
	   
	   //Validate the toast message
	   wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span//a[@title=\"Bootcamp\"]/ancestor::span[\"Task was created.\"]")));
	 
	   //driver.findElement(By.xpath("//div//span[text()=\"Task\"]//a//div[text()=\"Bootcamp\"]"));
	    
	  //click task again
	  element = driver.findElement(By.xpath("//one-app-nav-bar-item-root[contains(@class,\"navItem\")]//a[@title=\"Tasks\"]"));
	  executor = (JavascriptExecutor)driver;
	  executor.executeScript("arguments[0].click();", element);
	  	
	  //Select recently viewed tab
	    Thread.sleep(3000);
	    driver.findElement(By.xpath("//h1//span[text()='Recently Viewed']")).click();
	    Thread.sleep(3000);
	  //Select recently viewed option in the dropdown
	    driver.findElement(By.xpath("//a[@role=\"option\"]//span[text()='Recently Viewed']")).click();
	    Thread.sleep(3000);
	   
	 // Click the first task
	 //driver.findElement(By.xpath("//a//span[text()='Bootcamp']")).click();
	 	element = driver.findElement(By.xpath("//a//span[text()='Bootcamp']"));
	 	executor = (JavascriptExecutor)driver;
	 	executor.executeScript("arguments[0].click();", element);
	 		
	   // Assert the source and contact from the first task
	 	String source = driver.findElement(By.xpath("(//li[@data-aura-class=\"forceSplitViewListRecord\"]//a//span/span)[1]")).getText();
	    String name = driver.findElement(By.xpath("(//li[@data-aura-class=\"forceSplitViewListRecord\"]//a//span/span)[2]")).getText();
	 	Assert.assertEquals(name, "Natrayan");
	    Assert.assertEquals(source,"Bootcamp");
	    
	    
	    //System.out.println("Text written on the login button is- " + name);
	    
	}

}
