package week1Class1;

import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import io.github.bonigarcia.wdm.WebDriverManager;

public class editContract {

	public static void main(String[] args) throws InterruptedException {

		WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		
		ChromeDriver driver = new ChromeDriver(options);
	
		//1) Launch the browser
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		WebDriverWait wait=new WebDriverWait(driver, 20);
		
		//2) Click Login
		//3) Login with the credentials
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
				
		//4) Click on App launcher
		driver.findElement(By.xpath("//div[@class=\"slds-icon-waffle\"]")).click();
		//Click "View All"
		driver.findElement(By.xpath("//button[@aria-label=\"View All Applications\"]")).click();
		
		//scroll the page to element contracts
		WebElement element = driver.findElement(By.xpath("//span//p[text()=\"Contracts\"]")); 
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].scrollIntoView(true);", element); 
		Thread.sleep(500);
		executor.executeScript("arguments[0].click();", element);
		
		//enter the contract id and serach for it.
		driver.findElement(By.xpath("//input[@placeholder=\"Search this list...\"]")).sendKeys("00000168");
		driver.findElement(By.xpath("//input[@placeholder=\"Search this list...\"]"));
		Actions keyDown = new Actions(driver);
	    keyDown = new Actions(driver);
		Thread.sleep(2000);
		//this worked
		keyDown.sendKeys(Keys.ENTER).build().perform();
		
		 //find the table
		driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]"));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@aria-label=\"Recently Viewed\"]")));
		
		Thread.sleep(3000);
		
		//Get the list of rows
		List<WebElement> elements_row = driver.findElements(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr"));
		System.out.println("Rows in table are " + elements_row.size());
		
		//iterate rows and find the name, contract_id, start_date and end_date for each contract/row
		for (int i=1; i<=elements_row.size(); i++) {
			//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).getText());
			String contract_id_tb = driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).getText();
			//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[3]/span/a")).getText());
			String contract_name = driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[3]/span/a")).getText();
			//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[5]/span")).getText());
			String contract_st_date = driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[5]/span")).getText();
			//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[6]/span")).getText());
			String contract_end_date=driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[6]/span")).getText();
			System.out.println("Contract Name is "+contract_name+ " Contract Id is "+contract_id_tb+" contract start date is "+ contract_st_date + " contract end date is "+contract_end_date);
			
			//If contract in the table matches the required contract, click on the more action
			if (contract_id_tb.equals("00000168")) {
				element = driver.findElement(By.xpath("//tbody/tr[1]//a[text()=\"00000168\"]/ancestor::tr//span[text()=\"Show Actions\"]"));
				executor = (JavascriptExecutor)driver;
			    executor.executeScript("arguments[0].click();", element);
			    // Click Edit option
			    Thread.sleep(1000);
			    driver.findElement(By.xpath("//div[@role=\"menu\"]//a[@title=\"Edit\"]")).click();
			    //click the status dropdown
			    driver.findElement(By.xpath("(//div[@class=\"uiMenu\"]/div/div/div/a[@class='select'])[1]")).click();
				//Select status as Activated
				driver.findElement(By.xpath("//div[@role=\"menu\"]/ul/li/a[text()='Activated']")).click();
				Thread.sleep(1000);
				//click owner expiration notice
				driver.findElement(By.xpath("(//div[@class=\"uiMenu\"]/div/div/div/a[@class='select'])[2]")).click();
				//select 30 days
				driver.findElement(By.xpath("//div[@role=\"menu\"]/ul/li/a[text()='30 Days']")).click();
				//click save to save the changes.
				driver.findElement(By.xpath("//button[@title='Save']/span[text()='Save']")).click();
				
				//capture the toast message
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(@class,\"toastMessage\")]")));
			    String contract_toast = driver.findElement(By.xpath("//span[contains(@class,\"toastMessage\")]")).getText();
			    System.out.println(contract_toast);
			    if (contract_toast.contains("was saved")) {
			    	System.out.println("contract is saved");
			    }
			    
				System.out.println("contract is updated");
				
			}
		}
		
			//enter the contract id and serach for it.
			driver.findElement(By.xpath("//input[@placeholder=\"Search this list...\"]")).clear();
			driver.findElement(By.xpath("//input[@placeholder=\"Search this list...\"]")).sendKeys("00000168");
			driver.findElement(By.xpath("//input[@placeholder=\"Search this list...\"]"));
			keyDown = new Actions(driver);
		    keyDown = new Actions(driver);
			Thread.sleep(2000);
			//this worked
			keyDown.sendKeys(Keys.ENTER).build().perform();
			
			 //find the table
			driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]"));
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@aria-label=\"Recently Viewed\"]")));
			
			Thread.sleep(3000);
			
			//Get the list of rows
			elements_row = driver.findElements(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr"));
			System.out.println("Rows in table are " + elements_row.size());
			
			//iterate rows and find the name, contract_id, start_date and end_date for each contract/row
			for (int i=1; i<=elements_row.size(); i++) {
				//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).getText());
				String contract_id_tb = driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+ i +"]/th/span/a")).getText();
				//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[3]/span/a")).getText());
				String contract_name = driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[3]/span/a")).getText();
				//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[5]/span")).getText());
				String contract_st_date = driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[5]/span")).getText();
				//System.out.println(driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[6]/span")).getText());
				String contract_end_date=driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[6]/span")).getText();
				System.out.println("Contract Name is "+contract_name+ " Contract Id is "+contract_id_tb+" contract start date is "+ contract_st_date + " contract end date is "+contract_end_date);
				String contract_status=driver.findElement(By.xpath("//table[@aria-label=\"Recently Viewed\"]/tbody/tr["+i+"]/td[4]/span/span")).getText();
				System.out.println("contract_status is "+contract_status);
				//If contract in the table matches the required contract, click on the more action
				if (contract_id_tb.equals("00000168")) {
					System.out.println("contract is updated");
					
				}
				
		}
	}
	
}
