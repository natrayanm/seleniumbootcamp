package week1Class1;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import io.github.bonigarcia.wdm.WebDriverManager;

public class deleteTask {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		
		ChromeDriver driver = new ChromeDriver(options);
		
		
		//1) Launch the browser
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		WebDriverWait wait=new WebDriverWait(driver, 20);
		
		//2) Click Login
		//3) Login with the credentials
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		
		//4) Click on App launcher
		driver.findElement(By.xpath("//div[@class=\"slds-icon-waffle\"]")).click();
		//Click "View All"
		driver.findElement(By.xpath("//button[@aria-label=\"View All Applications\"]")).click();
		//Serach for application containing sale
		driver.findElement(By.xpath("//input[@placeholder=\"Search apps or items...\"]")).sendKeys("sale");
		System.out.println(driver.findElement(By.xpath("//p[@title=\"Manage your sales process with accounts, leads, opportunities, and more\"]")).getText());
		//Select the sales App
		driver.findElement(By.xpath("//p[@title=\"Manage your sales process with accounts, leads, opportunities, and more\"]")).click();
		
		//click task
		WebElement element = driver.findElement(By.xpath("//one-app-nav-bar-item-root[contains(@class,\"navItem\")]//a[@title=\"Tasks\"]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
	    
	  //Select recently viewed tab
	    Thread.sleep(3000);
	    driver.findElement(By.xpath("//h1//span[text()='Recently Viewed']")).click();
	    Thread.sleep(3000);
	  //Select recently viewed option in the dropdown
	    driver.findElement(By.xpath("//a[@role=\"option\"]//span[text()='Recently Viewed']")).click();
	    Thread.sleep(3000);
	   
	   
	    // Click the first task
	    element = driver.findElement(By.xpath("//a//span[text()='Bootcamp']"));
	    executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
	    Thread.sleep(3000);

	    //Expand the arrow for more actions
	    element = driver.findElement(By.xpath("//div//a[contains(@title,' more actions')]"));
	    executor = (JavascriptExecutor)driver;
	    executor.executeScript("arguments[0].click();", element);
	    //driver.findElement(By.xpath("//span[text()='Show 14 more actions']")).click();
		
	    //click the delete button
		driver.findElement(By.xpath("//ul//li//a[@title=\"Delete\"]")).click();
		driver.findElement(By.xpath("//button//span[text()=\"Delete\"]")).click();
		
		//System.out.println((driver.findElement(By.xpath("//div//span//a[@title=\"Undo\"]")).getText()));
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div//span//a[@title=\"Undo\"]")));
		 
		String toast_msg = driver.findElement(By.xpath("//div//span//a[@title=\"Undo\"]/ancestor::span")).getText();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div//span//a[@title=\"Undo\"]/ancestor::span")));
		
		Assert.assertEquals(toast_msg, "Task \"Bootcamp\" was deleted. Undo");

	}

}
